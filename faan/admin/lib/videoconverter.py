import subprocess
import sys
import os
import json
from subprocess import check_output, CalledProcessError

FRAME_COUNT = 10

resolutions = [ (1920, 1080)                        #, (1920, 1200) 
              , (1600, 1200)
              , (1280, 720)                         #, (1280, 800), (1280, 752),  (1232, 800)
              , (1024, 768)                         #, (1024, 720), (1024, 600), (1024, 552), (1003, 800)
                                                    #, (976, 768), (976, 600), (962, 601), (962, 553), (960, 540), (906, 601)
              , (800, 480)                          #, (854, 480), (800, 600), (762, 480)
              , (640, 360), (568, 320), (534, 320)  #, (592, 360), (570, 320), (519, 360), (503, 360)
              , (480, 360), (480, 320)              #, (487, 342), (471, 337), (455, 320), (416, 320), (405, 300)
              , (382, 240), (320, 240)              #, (387, 282), (371, 300), (349, 282), (320, 218), (305, 290)
                                                    #, (298, 240), (282, 234), (280, 234), (274, 240), (272, 240), (245, 228), (240, 228), (218, 189)
              ]

FFPROBE_CMD = "/usr/local/bin/ffprobe -v quiet -print_format json -show_streams"

FFMPEG_CMD = "/usr/local/bin/ffmpeg "   + \
             "-i %(src)s "              + \
             "-f mp4 "                  + \
             "-y "                      + \
             "-vf scale='if(gt(a,%(resw)s/%(resh)s),%(resw)s,trunc(oh*a/2)*2)':'if(gt(a,%(resw)s/%(resh)s),trunc(ow/a/2)*2,%(resh)s)' "              + \
             "-vcodec libx264 "         + \
             "-vprofile high10 "      + \
             "-x264opts keyint=30 "     + \
             "-acodec libfaac "         + \
             "%(dst)s"


def ffprobe(fname):
    w = h = None
    
    for s in check_output(FFPROBE_CMD.split(' ') + [fname]).split('\n'):
        if '"width"' in s:
            w = int(s.split(':')[1].strip(' ,.:'))
        if  '"height"' in s:
            h = int(s.split(':')[1].strip(' ,.:'))
    
    return (w, h)
        
    
def ffmpeg(src, dst, res):
    cmd = FFMPEG_CMD %  { 'src' : src
                        , 'dst' : dst
                        , 'resw': res[0]
                        , 'resh': res[1]
                        }

    if __name__ == '__main__':
        print "Running cmd[%s]" % cmd
    
    subprocess.Popen(cmd.split(' ')).wait()
    
    
def vconvert(path, ress, cress):
    for res in ress:
        if res in cress:
            continue
        
        ffmpeg( os.path.join(path, "source")
              , os.path.join(path, "video_%s_%s" % res)
              , res)

def vcopy(path, tgt_res, cress):
    for res in cress:
        try:
            os.unlink(os.path.join(path, "video_%s_%s" % res))
        except:
            pass
            
        os.symlink(os.path.join(path, "video_%s_%s" % tgt_res), os.path.join(path, "video_%s_%s" % res))
       

def report(mid):
    args ="/usr/local/bin/wget -qO- %s &> /dev/null" % "http://api.vidiger.com/util/converter_report?media=" + str(mid) + "&status=ok"
    subprocess.Popen(args.split(' '), close_fds=True)
    

def vconvert_process(path):
    args = ["nohup", "/usr/local/bin/python", os.path.realpath(__file__), path]
    subprocess.Popen(args, cwd=path, close_fds=True)


def get_duration(path):
    args = FFPROBE_CMD + " %s/source.mp4" % path
    output = json.loads(check_output(args.split(' ')))
    return float(output['streams'][0]['duration'])


if __name__ == '__main__':
    path = sys.argv[1]
    copy_res = []
    tgt_res = None
    last_res = None
    
    (w, h) = ffprobe(os.path.join(path, "source"))
    
    for (rw, rh) in resolutions:
        if rw > w or rh > h:
            copy_res.append((rw, rh))
            last_res = (rw, rh)
        else:
            tgt_res = tgt_res or (rw, rh)
    
    if tgt_res is None:
        tgt_res = last_res
        copy_res = copy_res[:-1]
        
    
    vconvert(path, resolutions, copy_res)
    vcopy(path, tgt_res, copy_res)

    # Get duration of the video
    duration = int(get_duration(path))
    # Create directory where the frames will be stored
    os.makedirs(path + '/frames')
    # Get frame every FRAME_COUNT seconds and save them into frame directory
    frame_count = FRAME_COUNT if duration > FRAME_COUNT else int(duration)
    FFMPEG_MAKE_FRAMES = "/usr/local/bin/ffmpeg -i {path}/video_1024_768 -f " \
                         "image2 -vf fps=fps={frame}/{duration} " \
                         "{path}/frames/frame%d.png".format(frame=frame_count,
                                                           path=path,
                                                           duration=duration)
    subprocess.Popen(FFMPEG_MAKE_FRAMES.split(' ')).wait()
    # Resize frames for preview.
    FFMPEG_MAKE_PREVIEW = "/usr/local/bin/ffmpeg -i {path}/frames/frame5.png " \
                          "-vf scale=120:90 {path}/frames/preview.png"\
        .format(path=path)
    subprocess.Popen(FFMPEG_MAKE_PREVIEW.split(' ')).wait()

    for i in xrange(1, FRAME_COUNT + 1):
        FFMPEG_MAKE_PREVIEW = "/usr/local/bin/ffmpeg -i " \
                              "{path}/frames/frame{i}.png " \
                              "-vf scale=120:90 {path}/frames/preview{i}.png"\
            .format(path=path, i=i)
        subprocess.Popen(FFMPEG_MAKE_PREVIEW.split(' ')).wait()
    report(os.path.basename(path))