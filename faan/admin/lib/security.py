from repoze.what.predicates import Predicate
from faan.core.model.security.account import Account


def get_account_id(environ):
    account_id = environ.get('repoze.who.identity', {}).get('repoze.who.userid', None)
    return account_id


def get_account(environ, account_id=None):
    if not account_id:
        account_id = get_account_id(environ)

    account = Account.Get(account_id)

    return account

                                
class IsSigned(Predicate):
    message = "Stop right there, criminal scum!"
    
    def __init__(self, **kwargs):
        Predicate.__init__(self, **kwargs)
    
    def evaluate(self, environ, credentials):
        if not get_account_id(environ):
            self.unmet()

class IsAdmin(Predicate):
    message = "Stop right there, criminal scum!"
    
    def __init__(self, **kwargs):
        Predicate.__init__(self, **kwargs)



    def evaluate(self, environ, credentials):
        account_id = get_account_id(environ)
        
        if not account_id:
            self.unmet()
            
        account = get_account(environ)
        if not account:
            self.unmet()
            
        #if acc.type < Account.Type.ADMINISTRATOR: # 20.5.2014 - limeschnaps
        if account.type < Account.Type.MANAGER:
            self.unmet()


class HasType(Predicate):
    message = "Halt! Du bist ein schmetterling!"

    def __init__(self, account_type, *account_types, **kwargs):
        Predicate.__init__(self, **kwargs)
        self.account_types = [account_type] + list(account_types)

    def evaluate(self, environ, credentials):
        account_id = get_account_id(environ) or self.unmet()
        account = get_account(environ) or self.unmet()
        account.type in self.account_types or self.unmet()