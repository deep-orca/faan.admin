# -*- coding: utf-8 -*-
import time
import urllib
from urlparse import urlunparse
import datetime

from webhelpers.html import tags
from faan.admin.controllers import permissions
from faan.admin.controllers.support import get_total_new_replies
from faan.admin.controllers.partners import get_total_new_publishers, \
    get_total_new_advertisers
from faan.admin.controllers.media import get_total_new_media, \
    get_total_new_media_video, get_total_new_media_banner, \
    get_total_new_media_mraid
from faan.admin.controllers.application import get_total_new_app
from faan.core.X.helpers.menu import Menu, MenuItem
from faan.core.model.meta import Session

from faan.x.util.timestamp import TimeStamp
from faan.x.util.func import try_int


def url_append(url, params=None, params2=None):
    if params is None:
        params = []
    if params2 is None:
        params2 = []
    if type(params) == dict:
        params = params.items()
    if type(params2) == dict:
        params2 = params2.items()
    qs = urllib.urlencode(dict(params + params2))
    return urlunparse(['', '', url, '', qs, '', ])


def divbyz(x, y):
    try:
        perc = float(x) / y
    except Exception:
        perc = 0
    return perc


# self.USER = 1
# self.PARTNER = 2
# self.MANAGER = 100
# self.FINANCIAL_MANAGER = 120
# self.ADMINISTRATOR = 150
# self.DEVELOPER = 200
# self.GOD = 1000

admin_menu = Menu([
    MenuItem(u"Главная", url="/", icon="home", for_types=permissions.index.account_types),
    MenuItem(u"Кампании", url="/campaign/", icon="bullhorn", for_types=permissions.campaigns.account_types),
    MenuItem(u"Креативы", url="/media/", icon="video-camera", for_types=permissions.media.account_types, sub_items=[
        MenuItem(u"Список", url="/media/list/", icon="list"),
        MenuItem(u"Видео", url="/media/video/", icon="video-camera",
                 extra_html=lambda: "<span class='badge pull-right'>{medias}</span>".format(medias=get_total_new_media_video() or '')),
        MenuItem(u"Баннер", url="/media/banner/", icon="image",
                 extra_html=lambda: "<span class='badge pull-right'>{medias}</span>".format(medias=get_total_new_media_banner() or '')),
        MenuItem(u"MRAID", url="/media/mraid/", icon="archive",
                 extra_html=lambda: "<span class='badge pull-right'>{medias}</span>".format(medias=get_total_new_media_mraid() or '')),
    ], extra_html=lambda: "<span class='badge pull-right'>{medias}</span>".format(medias=get_total_new_media() or '')),
    MenuItem(u"Площадки", url="/app/", icon="rocket", for_types=permissions.applications.account_types,
             extra_html=lambda: "<span class='badge pull-right'>{apps}</span>".format(apps=get_total_new_app() or ''), sub_items=[
            MenuItem(u"Площадки", url="/app/", icon="rocket",
                     extra_html=lambda: "<span class='badge pull-right'>{apps}</span>".format(apps=get_total_new_app() or '')),
            MenuItem(u"Блоки", url="/unit/", icon="cube"),
            MenuItem(u"Источники", url="/source/", icon="list-ul"),
        ]),
    MenuItem(u"Пользователи", url="/profile/", icon="smile-o", sub_items=[
        MenuItem(u"Список", url="/profile/list/", icon="list"),
        MenuItem(u"Создать", url="/profile/create/", icon="plus")
    ], for_types=permissions.profiles.account_types),
    MenuItem(u"Партнеры", url="/partners/", icon="group", sub_items=[
        MenuItem(u"Разработчики", url="/partners/pub/", icon="mobile-phone",
                 extra_html=lambda: "<span class='badge pull-right'>{users}</span>".format(users=get_total_new_publishers() or '')),
        MenuItem(u"Рекламодатели", url="/partners/adv/", icon="desktop",
                 extra_html=lambda: "<span class='badge pull-right'>{users}</span>".format(users=get_total_new_advertisers() or '')),
        MenuItem(u"Группы", url="/partners/group/", icon="th")
    ], for_types=permissions.partners.account_types,
             extra_html=lambda: "<span class='badge pull-right'>{users}</span>".format(users=get_total_new_publishers() + get_total_new_advertisers() or '')),
    MenuItem(u"Статистика", url="/stats/", icon="bar-chart-o", sub_items=[
        MenuItem(u"Разработчики", url="/stats/pub/", icon="mobile-phone", sub_items=[
            MenuItem(u"По часам", url="/stats/pub/hours/", icon="clock-o"),
            MenuItem(u"По дням", url="/stats/pub/days/", icon="calendar"),
            MenuItem(u"По разработчикам", url="/stats/pub/pub/", icon="user"),
            MenuItem(u"По приложениям", url="/stats/pub/app/", icon="rocket"),
            MenuItem(u"По блокам", url="/stats/pub/unit/", icon="cube"),
            MenuItem(u"По источникам", url="/stats/pub/source/", icon="sitemap"),
            MenuItem(u"По гео", url="/stats/pub/geo/", icon="globe", sub_items=[
                MenuItem(u"По странам", url="/stats/pub/geo/country/", icon="flag"),
                MenuItem(u"По регионам", url="/stats/pub/geo/region/", icon="picture-o"),
                MenuItem(u"По городам", url="/stats/pub/geo/city/", icon="building"),
            ]),
        ]),
        MenuItem(u"Рекламодатели", url="/stats/adv/", icon="desktop", sub_items=[
            MenuItem(u"По часам", url="/stats/adv/hours/", icon="clock-o"),
            MenuItem(u"По дням", url="/stats/adv/days/", icon="calendar"),
            MenuItem(u"По разработчикам", url="/stats/adv/adv/", icon="user"),
            MenuItem(u"По кампаниям", url="/stats/adv/cam/", icon="rocket"),
            MenuItem(u"По медиа", url="/stats/adv/media/", icon="sitemap"),
            MenuItem(u"По гео", url="/stats/adv/geo/", icon="globe", sub_items=[
                MenuItem(u"По странам", url="/stats/adv/geo/country/", icon="flag"),
                MenuItem(u"По регионам", url="/stats/adv/geo/region/", icon="picture-o"),
                MenuItem(u"По городам", url="/stats/adv/geo/city/", icon="building"),
            ]),
        ])
    ], for_types=permissions.stats.account_types),
    MenuItem(u"Новости", url="/news/", icon="inbox", for_types=permissions.news.account_types),
    MenuItem(u"Финансы", url="/balance/", icon="ruble", for_types=permissions.balance.account_types, sub_items=[
        MenuItem(u'История платежей', url="/balance/", icon="ruble"),
        MenuItem(u'Доходы', url="/finances/", icon="money"),
    ]),
    MenuItem(u"Поддержка", url="/support/", icon="life-ring", for_types=permissions.support.account_types,
             extra_html=lambda: "<span class='badge pull-right'>{new_tickets}</span>".format(new_tickets=sum([v for k, v in dict(get_total_new_replies()).iteritems()]) or "")),
    MenuItem(u"SDK", url="/sdk/log/", icon="keyboard-o", sub_items=[
        MenuItem(u"Лог ошибок", url="/sdk/log/", icon="bug")
    ], for_types=permissions.sdk.account_types),
    MenuItem(u"Настройки", url="/settings/", icon="gear", for_types=permissions.settings.account_types),
])