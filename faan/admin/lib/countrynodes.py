# -*- coding: utf-8 -*-
from faan.core.model.general.geo import GeoRegion, GeoCity, Geo, GeoCountry
from flask import json
import redis


def generateNodes():
    countries = GeoCountry.Query().order_by(GeoCountry.id).all()
    regions = GeoRegion.Query().order_by(GeoRegion.country, GeoRegion.id).all()
    cities = GeoCity.Query().order_by(GeoCity.region, GeoCity.id).all()

    hashred = lambda sx: reduce(lambda a, b: a ^ b, sx, 0)

    xh = hashred([c.id for c in countries]) ^ hashred([r.id ^ r.country for r in regions]) ^ hashred(
        [c.id ^ c.region for c in cities])

    r = redis.StrictRedis(host="vidiger.com")
    redis_key = "CountryNodes_%s" % xh
    ret = r.get(redis_key)

    # print "Key: [%s]" % redis_key
    #print ret[:40] if ret else "RET-NONE"

    # if ret is not None:
    #     return ret

    root_node = {
        'title': u'Все страны',
        'key': Geo(country=Geo.COUNTRY_ALL, region=Geo.REGION_ALL, city=Geo.CITY_ALL).value,
        'folder': True,
        'children': [],
    }

    for country in countries:
        country_node = {
            'title': country.name,
            'key': Geo(country=country.id, region=Geo.REGION_ALL, city=Geo.CITY_ALL).value,
            'folder': True,
            'children': [],
            'data': {
                'multiplier': float(country.multiplier),
                'type': 'country',
                'id': country.id
            }
        }

        for region in regions:
            if region.country != country.id: continue
            region_node = {
                'title': region.name,
                'key': Geo(country=country.id, region=region.id, city=Geo.CITY_ALL).value,
                'folder': True,
                'children': [],
                'data': {
                    'multiplier': float(region.multiplier),
                    'type': 'region',
                    'id': region.id
                }
            }

            for city in cities:
                if city.region != region.id: continue
                city_node = {
                    'title': city.name,
                    'key': Geo(country=country.id, region=region.id, city=city.id).value,
                    'data': {
                        'multiplier': float(city.multiplier),
                        'type': 'city',
                        'id': city.id
                    }
                }

                region_node['children'].append(city_node)
            country_node['children'].append(region_node)
        root_node['children'].append(country_node)

    ret = json.dumps(root_node['children'])

    #print r.set(redis_key, ret)

    return ret
