from faan.core.X.xflask import XFlask
from faan.core.X.mako import init_mako
from faan.core.X.xemail import init_email
from . import controllers


def make_app(global_conf, **app_conf):
    app = XFlask(__name__,  static_folder=app_conf['STATIC_FOLDER'])
    app.debug = True
    app.find_controllers(app.root_path + "/controllers/", controllers.__name__)
    app.config.update(app_conf)
    init_mako(app)
    init_email(app)
    return app.wsgi_app

if __name__ == "__main__":
    app = make_app(None)
    app.run()