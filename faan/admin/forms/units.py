# coding=utf-8
from gettext import gettext
from wtforms import StringField, IntegerField, SelectField, BooleanField
from wtforms.validators import url, input_required, optional, required, data_required, length
from wtforms.widgets import HiddenInput
from faan.core.model.pub.unit import Unit
from faan.web.forms import XForm, XSelectMultipleField


class UnitForm(XForm):

    SIZES = [
        (Unit.Size.BANNER, u'320 &times; 50'),
        (Unit.Size.LARGE_BANNER, u'320 &times; 100'),
        (Unit.Size.MEDIUM_RECTANGLE, u'300 &times; 250'),
        (Unit.Size.FULL_BANNER, u'468 &times; 60'),
        (Unit.Size.LEADERBOARD, u'728 &times; 90'),
    ]

    type = IntegerField(u"Тип блока", widget=HiddenInput())

    name = StringField(u"Название",
                       validators=[
                           data_required(),
                           length(max=64, message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')
                       ])

    session_limit = IntegerField(u"Показов на пользователя за сессию",
                                 validators=[optional()],
                                 default=0,
                                 description=u"Сколько раз реклама может быть показана пользователю за один запуск приложения. <br>0 - без ограничений.")
    daily_limit = IntegerField(u"Показов на пользователя за сутки",
                               validators=[optional()],
                               default=0,
                               description=u"Сколько раз реклама может быть показана пользователю за сутки. <br>0 - без ограничений.")
    mod_factor = IntegerField(u"Кратность показов", validators=[optional()], default=0,
                              description=u"0, 1 - реклама показывается каждый вызов, 2 - каждый второй вызов, и т.д.")

    width = IntegerField(validators=[input_required()], widget=HiddenInput())
    height = IntegerField(validators=[input_required()], widget=HiddenInput())

    refresh_rate = IntegerField(u"Период обновления", description=u"Время в секундах, через которое в блоке появится новый баннер <br> 0 - не обновляется")
