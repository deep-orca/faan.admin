# coding=utf8

from decimal import Decimal
from wtforms import ValidationError
from wtforms import StringField, SelectField, BooleanField, \
    PasswordField, DecimalField, TextAreaField
from wtforms.validators import optional, required, Email, EqualTo
from faan.admin.forms import XForm
from faan.core.model.security.account import Account


class UniqueEmailValidator(object):
    """
    Validates an email address. This validator confirms that
    the email is unique.

    :param message:
        Error message to raise in case of a validation error.
    """
    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field):
        if Account.All(email=field.data):
            message = self.message
            if message is None:
                message = field.gettext('This email already exists')
            raise ValidationError(message=message)


class GreaterOrEqualValidator(object):
    """
    Validates an number. This validator confirms that the number greater or
    equal than some value.

    :param value:
        The field has to be greater or equal than this value.
    :param message:
        Error message to raise in case of a validation error.
    """
    def __init__(self, value, message=None):
        self.value = value
        self.message = message

    def __call__(self, form, field):
        if Decimal(field.data) < Decimal(self.value):
            message = self.message
            if message is None:
                message = field.gettext('The value has to be '
                                        'greater or equal that {0}'
                                        .format(self.value))
            raise ValidationError(message=message)


class UserForm(XForm):

    email = StringField(u'Почтовый адрес', validators=[required(), Email(),
                                                       UniqueEmailValidator()])
    password = PasswordField(u'Введите пароль', validators=[required()])
    password_confirm = PasswordField(u'Введите пароль',
                                     validators=[required(),
                                                 EqualTo('password')])
    name = StringField(u'Имя', validators=[optional()])
    surname = StringField(u'Фамилия', validators=[optional()])
    balance = DecimalField(u'Баланс',
                           validators=[optional(), GreaterOrEqualValidator(0)],
                           default=0)
    type = SelectField(u'Account Type', choices=[
        (Account.Type.PARTNER, u'Партнер'),
        (Account.Type.USER, u'Пользователь'),
        (Account.Type.MANAGER, u'Менеджер'),
        (Account.Type.FINANCIAL_MANAGER, u'Финансовый менеджер'),
        (Account.Type.ADMINISTRATOR, u'Администратор'),
        (Account.Type.DEVELOPER, u'Разработчик'),
        (Account.Type.GOD, u'Супер пользователь'),
    ], default=Account.Type.PARTNER, coerce=int)
    group = SelectField(u'Account Group', choices=[
        (Account.Groups.ADV, u'Рекламодатель'),
        (Account.Groups.PUB, u'Разработчик'),
        (Account.Groups.NONE, u'Без группы'),
    ], default=Account.Groups.ADV, coerce=int)
    campaign = StringField(u'Campaign', validators=[optional()])
    description = TextAreaField(u'Description', validators=[optional()])
    send_reg_msg = BooleanField(u'Send a registration message',
                                validators=[optional()])