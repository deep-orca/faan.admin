# coding=utf-8
from decimal import Decimal
from wtforms import DecimalField, StringField, IntegerField, ValidationError
from wtforms.validators import input_required
from faan.admin.forms import XForm

__author__ = 'limeschnaps'


class MultiplierEditForm(XForm):
    geo_type = StringField("geo_type", validators=[input_required()])
    geo_id = IntegerField("geo_id", validators=[input_required()])
    multiplier = DecimalField("multiplier", default=Decimal(1), validators=[input_required()])

    def validate_multiplier(self, field):
        if field.data <= 0:
            raise ValidationError(u"Коэффицент должен быть больше нуля")

    def validate_geo_type(self, field):
        if field.data not in ['country', 'region', 'city']:
            raise ValidationError("Invalid geo type")

    def validate_geo_id(self, field):
        pass