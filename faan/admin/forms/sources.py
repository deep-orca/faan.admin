# coding=utf-8
from gettext import gettext
from wtforms import Form
from wtforms import StringField, IntegerField, SelectField, BooleanField, \
    DecimalField, TextField
from wtforms.fields import FormField, FieldList
from wtforms.validators import url, input_required, optional, required, data_required, length
from wtforms.widgets import HiddenInput
from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source import Source
from faan.core.model.adv.media import Media
from faan.admin.forms import XForm


# TODO: Replace with singleton
class _SourceForm(object):
    """Factory to create a source with specific type."""

    """Supported source types"""
    Types = (
        (Source.Type.VIDIGER, 'VIDIGER'),
        (Source.Type.ADMOB, 'ADMOB'),
    )

    def __call__(self, *args, **kwargs):
        # Get source instance and determine its type.
        source = kwargs.get('obj')
        if source.type == Source.Type.VIDIGER:
            form = SourceFormVidiger(*args, **kwargs)
        elif source.type == Source.Type.ADMOB:
            form = SourceFormAdmob(*args, **kwargs)
        else:
            raise ValueError('Unknown source type!')
        return form


SourceForm = _SourceForm()


class SourceFormBase(XForm):
    """Base source form. All source form subclasses have to inherit it."""

    name = StringField(u"Название", validators=[data_required(),
        length(max=64, message=u'Превышено допустимое количество символов в '
                               u'названии. Максимум 64 знака')])

    type = IntegerField(u"Тип источника", validators=[input_required()],
                        widget=HiddenInput())
    ecpm = DecimalField(u"ECPM", validators=[input_required()], default=0)

    def fill_source(self, application, unit, source):
        """ Fills the source with data from a form.
            :param application:
                An instance of Application class.
            :param unit:
                An instance of Unit class.
            :param source:
                An instance of Source class.
        """
        source.unit = unit.id
        source.name = self.name.data
        source.type = self.type.data
        source.ecpm = self.ecpm.data
        source.poly = self.poly(application, unit, source)
        source.flags = source.flags or Source.Flag.NONE
        source.state = source.state or Source.State.ACTIVE
        return source

    def poly(self, application, unit, source):
        """ Determine the value of poly field.
            :param application:
                An instance of Application class.
            :param unit:
                An instance of Unit class.
            :param source:
                An instance of Source class.
        """
        return application.os * 10000 + unit.type * 1000 + source.type


class SourceFormVidiger(SourceFormBase):
    """The source that represents a Vidiger source."""

    banner = BooleanField(u'Banner', validators=[optional()])
    video = BooleanField(u'Video', validators=[optional()])
    mraid = BooleanField(u'MRAID', validators=[optional()])

    def fill_source(self, application, unit, source):
        """ Fills the source with data from a form. """
        super(SourceFormVidiger, self).fill_source(application, unit, source)
        types = []
        types += [Media.Type.BANNER] if self.banner.data else []
        types += [Media.Type.VIDEO] if self.video.data else []
        types += [Media.Type.MRAID] if self.mraid.data else []
        source.parameters = {'media_types': types}


class SourceFormAdmob(SourceFormBase):
    """The source that represents an Admob source."""

    # Unit ID of an Admob source.
    unit = StringField(u'Unit', validators=[required()])

    def fill_source(self, application, unit, source):
        """ Fills the source with data from a form. """
        super(SourceFormAdmob, self).fill_source(application, unit, source)
