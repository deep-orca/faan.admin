# coding=utf-8
from decimal import Decimal
from wtforms import DecimalField, StringField, IntegerField, ValidationError, TextField, SelectField
from wtforms.validators import input_required, length
from wtforms.widgets import TextArea, HiddenInput
from faan.admin.forms import XForm, XSelectMultipleField, TagField
from faan.core.model.news import News
from faan.core.model.tag import Tag


__author__ = 'limeschnaps'


class NewsEditForm(XForm):
    PRIORITY_CHOICES = (
        (News.Priority.LOW, u"Низкий"),
        (News.Priority.NORMAL, u"Обычный"),
        (News.Priority.HIGH, u"Высокий"),
        (News.Priority.CRITICAL, u"Критический"),
    )

    FOR_GROUP_CHOICES = (
        (News.ForGroup.ALL, u"Все"),
        (News.ForGroup.ADV, u"Рекламодатели"),
        (News.ForGroup.PUB, u"Разработчики"),
    )

    title = StringField(u"Заголовок", validators=[input_required(), length(max=100)], description=u"Максимальная длина 100 символов")
    priority = SelectField(u"Уровень важности", validators=[input_required()], choices=PRIORITY_CHOICES, coerce=int)
    for_group = SelectField(u"Кто видит", validators=[input_required()], choices=FOR_GROUP_CHOICES, coerce=int)
    message = TextField(u"Текст", validators=[input_required(), length(max=2044)], widget=TextArea(), description=u"Максимальная длина 2000 символов")
    tags = TagField(u"Теги", tag_model_cls=Tag, description=u"Указывайте теги через запятую или двойной пробел")
