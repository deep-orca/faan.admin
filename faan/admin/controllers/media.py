# -*- coding: utf-8 -*-
import os
import json
import requests
import subprocess
from sqlalchemy import or_
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.adv.campaign import Campaign
from faan.core.model.meta import Session
from faan.core.model.adv.media import Media
from faan.core.model.security.account import Account
from faan.admin.lib.videoconverter import vconvert_process
from faan.x.util.func import try_int
from faan.web.lib.helpers import SAJson
from faan.web.forms.advertiser import MediaBannerForm, MediaVideoForm, \
    MediaMraidForm, MraidOrderForm
from shutil import move


def _get_total_new_media(mtype=None):
    try:
        new_media = Session.query(Media).filter(Media.state == Media.State.NONE)
        if mtype is not None:
            new_media = new_media.filter(Media.type == mtype)
        return new_media.count()
    except Exception:
        return 0


def get_total_new_media():
    return _get_total_new_media()


def get_total_new_media_video():
    return _get_total_new_media(Media.Type.VIDEO)


def get_total_new_media_banner():
    return _get_total_new_media(Media.Type.BANNER)


def get_total_new_media_mraid():
    return _get_total_new_media(Media.Type.MRAID)


class MediaTabs(object):
    NEW = 1
    ACTIVE = 2
    BANNED = 3

    @classmethod
    def tabs(cls):
        return cls.NEW, cls.ACTIVE, cls.BANNED


@CProtect(IsSigned())
@CProtect(permissions.media)
@Controller('/media')
class MediaController():

    def _build_dicts(self, select=None, search=None):
        """Builds dictionaries."""

        q = Session.query(Account).order_by(Account.username)
        if select == 'account':
            q = q.filter(Account.username.ilike(u'%{0}%'.format(search)))
        g.accounts = q.all()

        q = Session.query(Campaign).order_by(Campaign.name)
        if select == 'campaign':
            q = q.filter(Campaign.name.ilike(u'%{0}%'.format(search)))
        g.campaigns = q.all()

        g.accounts_dict = dict([(account.id, (account.username, None))
                                for account in g.accounts])
        g.campaigns_dict = dict([(c.id, (c.name, c.account))
                                for c in g.campaigns])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements, light=False, select=None,
                        search=None):
        """Builds pipeline from 'select' elements. The pipeline means that
        elements dependencies each other."""
        pipeline = {}
        if not light:
            self._build_dicts(select=select, search=search)

        store = {
            'account': {
                'name': 'account',
                'id': 'select_account',
                'dict': None if light else g.accounts_dict,
            },
            'campaign': {
                'name': 'campaign',
                'id': 'select_campaign',
                'dict': None if light else g.campaigns_dict,
            },
        }

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline

    def _filter_media(self, start=None, count=None, search=None, mid=None,
                      fmtype=None, fmaction=None, fbanner_width=None,
                      fbanner_heigth=None):
        q = Session.query(Media, Campaign, Account) \
                   .filter(Account.id == Campaign.account,
                           Campaign.id == Media.campaign)\
                   .order_by(Media.name)

        if g.account and g.account.isdigit():
            q = q.filter(Account.id == g.account)

        if g.campaign and g.campaign.isdigit():
            q = q.filter(Campaign.id == g.campaign)

        if g.tab == MediaTabs.NEW:
            q = q.filter(Media.state == Media.State.NONE)
        elif g.tab == MediaTabs.ACTIVE:
            q = q.filter(Media.state.in_([Media.State.ACTIVE,
                                          Media.State.DISABLED]))
        elif g.tab == MediaTabs.BANNED:
            q = q.filter(Media.state == Media.State.SUSPENDED)

        if g.mtype is not None:
            q = q.filter(Media.type == g.mtype)

        if fmtype is not None:
            q = q.filter(Media.type == fmtype)

        if mid:
            q = q.filter(Media.id == mid)

        if fmaction is not None:
            q = q.filter(Media.action_click == fmaction)

        if fbanner_width is not None and fbanner_heigth is not None:
            q = q.filter(Media.width == fbanner_width,
                         Media.height == fbanner_heigth)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Media.name.ilike(u'%{0}%'.format(search)),
                    Media.uri.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Campaign.name.like(u'%{0}%'.format(search)),
                    Media.id == search,
                    Media.duration == search,
                ))
            else:
                q = q.filter(or_(
                    Media.name.ilike(u'%{0}%'.format(search)),
                    Media.uri.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Campaign.name.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    def _media(self, mtype=None):
        g.account = request.values.get('account', '')
        g.campaign = request.values.get('campaign', '')
        g.tab = try_int(request.values.get('tab', MediaTabs.NEW))
        g.media_id = try_int(request.values.get('mID'), '')
        g.media_action = json.dumps(try_int(
            request.values.get('action_on_click'), None))
        g.banner_size = json.dumps(try_int(
            request.values.get('banner_size'), None))
        elements = ['account', 'campaign']

        if g.account:
            account = Account.Key(g.account)
            g.account_name = account.username

        if g.campaign:
            campaign = Campaign.Key(g.campaign)
            g.campaign_name = campaign.name

        pipeline = self._build_pipeline(elements, light=True)
        g.selects = [pipeline[e] for e in elements]
        g.mtype = json.dumps(mtype)

    @Route("/video/")
    def media_video(self):
        self._media(Media.Type.VIDEO)
        return render_template('/media/video.mako')

    @Route("/banner/")
    def media_banner(self):
        self._media(Media.Type.BANNER)
        return render_template('/media/banner.mako')

    @Route("/mraid/")
    def media_mraid(self):
        self._media(Media.Type.MRAID)
        return render_template('/media/mraid.mako')

    @Route("/list/")
    def media_list(self):
        self._media()
        return render_template('/media/list.mako')

    def _disable_media(self, media_id, commit=True):
        Session.query(Media).filter(Media.id == media_id)\
            .update({"state": Media.State.SUSPENDED})
        if commit:
            Session.commit()

    def _active_media(self, media_id, commit=True):
        media = Media.Key(media_id)
        campaign = Campaign.Key(media.campaign)

        if media.state == Media.State.NONE and media.type == Media.Type.VIDEO:
            media_folder = '%s/%s/%s' % (campaign.account, campaign.id, media.id)
            destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

            capp.logger.debug("converting %s in [%s]", media.id, destination_folder)

            vconvert_process(destination_folder)

        Session.query(Media).filter(Media.id == media_id)\
            .update({"state": Media.State.ACTIVE})
        if commit:
            Session.commit()

    def _stop_media(self, media_id, commit=True):
        Session.query(Media).filter(Media.id == media_id)\
            .update({"state": Media.State.DISABLED})
        if commit:
            Session.commit()

    def _delete_media(self, media_id, commit=True):
        m = Media.Get(media_id)
        Session.delete(m)
        if commit:
            Session.flush()
            Session.commit()
        pass

    @Route("/active/<int:media_id>")
    def active_media(self, media_id=None):
        g.tab = request.values.get('tab', 'new')
        self._active_media(media_id)
        redirect_address = '/media?tab=' + g.tab
        return redirect(redirect_address)

    @Route("/disable/<int:media_id>")
    def disable_media(self, media_id=None):
        g.tab = request.values.get('tab', 'new')
        self._disable_media(media_id)
        redirect_address = '/media?tab=' + g.tab
        return redirect(redirect_address)

    @Route("/stop/<int:media_id>")
    def stop_media(self, media_id=None):
        g.tab = request.values.get('tab', 'new')
        self._stop_media(media_id)
        redirect_address = '/media?tab=' + g.tab
        return redirect(redirect_address)

    @Route("/ajax/select")
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]

        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))

        pipeline = self._build_pipeline(pipeline_elements, select=query,
                                        search=search)

        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            select = pipeline.get(name)
            if not select:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id and _id.isdigit():
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/table")
    def ajax_table(self):
        values = request.values.get('values', '')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')
        g.mtype = try_int(request.values.get('mtype'), None)
        g.tab = try_int(request.values.get('tab'))
        mid = try_int(request.values.get('mID'), None)
        fmtype = try_int(request.values.get('fmtype'), None)
        fmaction = try_int(request.values.get('mediaAction'), None)
        fbanner_width = try_int(request.values.get('bannerWidth'), None)
        fbanner_heigth = try_int(request.values.get('bannerHeigth'), None)

        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            setattr(g, name, _id if _id and _id.isdigit() else None)

        self._filter_media(start=start, count=count, search=search, mid=mid,
                           fmtype=fmtype, fmaction=fmaction,
                           fbanner_width=fbanner_width,
                           fbanner_heigth=fbanner_heigth)
        objects = {
            Media.Type.BANNER: self._media_table_banner,
            Media.Type.VIDEO: self._media_table_video,
            Media.Type.MRAID: self._media_table_mraid,
            None: self._media_table_list,
        }.get(g.mtype)()

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)

    def _media_table(self, update_row=None):
        objects = []
        for m, c, a in sorted(g.data, cmp=lambda x, y: -cmp(x[0].id, y[0].id)):
            media = {
                'id': m.id,
                'name': m.name,
                'account':  a.email,
                'account_id': a.id,
                'campaign': c.name,
                'campaign_id': c.id,
                'uri': m.uri,
                'duration': m.duration,
                'phone': m.phone_number,
                'mraid': m.mraid_file
            }
            d = {
                'checkbox': False,
                'media': json.dumps(media),
                'state': json.dumps({'id': m.state}),
                'action': None,
            }
            if update_row:
                update_row(self, d, m)
            objects.append(d)

        return objects

    def _media_table_list(self):
        def update(self, d, m):
            d.update({
                'type': m.type,
                'representation': json.dumps(self.media_representation(m)),
            })
        return self._media_table(update_row=update)

    def _media_table_video(self):
        def update(self, d, m):
            d.update({
                'endcard': m.endcard,
                'overlay': m.overlay,
                'video': json.dumps(self.media_representation(m)),
                'on_click': m.action_click,
            })
        return self._media_table(update_row=update)

    def _media_table_banner(self):
        def update(self, d, m):
            d.update({
                'banner': json.dumps(self.media_representation(m)),
                'banner_size': '{0}x{1}'.format(m.width, m.height),
                'on_click': m.action_click,
            })
        return self._media_table(update_row=update)

    def _media_table_mraid(self):
        return self._media_table()

    def media_representation(self, media):
        rep = {}
        if media.type == Media.Type.BANNER:
            rep.update({'image': media.banner, 'video': media.video,
                        'height': str(media.height),
                        'width': str(media.width)})
        elif media.type == Media.Type.VIDEO:
            rep.update({'id': str(media.id), 'video': media.video})
            # Check if the preview frame exists.
            preview_frame = '/global/static/img/icons/video_preview.png'
            if media.state != Media.State.NONE:
                preview_frame = media.video.replace('source',
                                                    'frames/preview.png')
                r = requests.head('http://vidiger.com' + preview_frame)
                if not r.status_code == requests.codes.ok:
                    preview_frame = '/global/static/img/icons/video_preview.png'
            rep['preview'] = preview_frame
        elif media.type == Media.Type.MRAID:
            pass
        return rep

    @Route("/group_actions", methods=["POST"])
    def group_actions(self):
        medias = request.values.get('objects_list')
        action = request.values.get('action')

        method = {
            'active': self._active_media,
            'disable': self._disable_media,
            'delete': self._delete_media,
            'stop': self._stop_media,
        }.get(action)

        if not method:
            return {}

        for m in list(set(medias.split(','))):
            method(m, commit=False)

        Session.flush()
        Session.commit()

        return "{}"

    @Route("/<int:media_id>", methods=["GET", "POST"])
    def form(self, media_id):
        g.media = Media.Single(id=media_id) or Media.Default()
        campaign_id = g.media.campaign
        g.campaign = Campaign.Key(g.media.campaign)
        
        mtype = {Media.Type.BANNER: 'banner', Media.Type.VIDEO: 'video', 
                 Media.Type.MRAID: 'mraid'}.get(g.media.type)
        tab = str({
            Media.State.ACTIVE: MediaTabs.ACTIVE, 
            Media.State.DISABLED: MediaTabs.ACTIVE,
            Media.State.SUSPENDED: MediaTabs.BANNED,
            Media.State.NONE: MediaTabs.NEW
        }.get(g.media.state))
        
        redirect_path = '/media/' + mtype + '/?tab=' + tab
        redirect_exc_path = '/media/list/'

        # Existing video values
        account_id = g.campaign.account
        campaign_name_list = {c.id: c.name for c in
                              Session.query(Campaign.id, Campaign.name).filter(Campaign.account == account_id).all()}
        media_list = Session.query(Media).filter(Media.campaign.in_(campaign_name_list.keys()), Media.type == Media.Type.VIDEO).all()
        videos = SAJson.serialize(media_list)
        for item in videos:
            item.update({'campaign_name': campaign_name_list.get(item.get('campaign'))})
        grouped_dict = {}
        for item in videos:
            if item.get('campaign_name') not in grouped_dict.keys():
                grouped_dict[item.get('campaign_name')] = []
            grouped_dict[item.get('campaign_name')].append(item)

        g.videos = [{'name': name, 'media': media} for name, media in grouped_dict.iteritems()]
        # End

        form_data = request.form
        try:
            form_type = int(form_data.get('type'))
        except:
            form_type = g.media.type

        banner_form = MediaBannerForm(obj=g.media)
        video_form = MediaVideoForm(obj=g.media)
        mraid_form = MediaMraidForm(obj=g.media)

        # BANNER
        if form_type == Media.Type.BANNER:
            form = MediaBannerForm(request.form, obj=g.media)
            banner_form = form

            if request.method == "POST" and form.validate():
                try:
                    g.media.name                = form.data.get('name')
                    g.media.campaign            = g.campaign.id
                    g.media.type                = Media.Type.BANNER

                    g.media.action_click        = form.data.get('action_click')

                    g.media.daily_limit         = form.data.get('daily_limit')
                    g.media.session_limit       = form.data.get('session_limit')

                    g.media.width = form.width.data
                    g.media.height = form.height.data

                    if (g.media.width, g.media.height) in [Media.BannerSizes.FULLSCREEN_PHONE(),
                                                           Media.BannerSizes.FULLSCREEN_TABLET()]:
                        g.media.placement = Media.Placement.INTERSTITIAL
                    else:
                        g.media.placement = Media.Placement.BANNER

                    Session.add(g.media)
                    Session.flush()

                    media_folder = '%s/%s/%s' % (request.environ['REMOTE_USER'], campaign_id, g.media.id)
                    destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

                    if not os.path.isdir(destination_folder):
                        os.makedirs(destination_folder)

                    if request.values.get('banner') and request.values.get('banner') != g.media.banner:
                        g.media.state = Media.State.NONE

                        g.media.banner = os.path.join('/global/upload', media_folder, 'banner')

                        temp_banner_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                        *(request.values['banner'].split('/')[-1:]))
                        vdst = os.path.join(destination_folder, 'banner')

                        capp.logger.debug("moving [" + temp_banner_path + "] -> [" + vdst + "]")

                        os.rename(temp_banner_path, vdst)

                    if g.media.action_click == Media.ClickActions.VIDEO:
                        if form.data.get('existing_video'):
                            existing_video              = Media.Key(form.data.get('existing_video'))
                            g.media.video               = existing_video.video
                            g.media.closable            = existing_video.closable
                            g.media.daily_limit         = existing_video.daily_limit
                            g.media.session_limit       = existing_video.session_limit
                            g.media.overlay             = existing_video.overlay
                            g.media.endcard             = existing_video.endcard
                            g.media.uri                 = existing_video.uri
                            g.media.uri_target          = existing_video.uri_target
                            g.media.duration            = existing_video.duration
                        else:
                            if request.values.get('video') and request.values.get('video') != g.media.video:
                                g.media.state = Media.State.NONE

                                temp_video_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                               *(request.values['video'].split('/')[-1:]))

                                vdst = os.path.join(destination_folder, 'source')

                                move(temp_video_path, vdst)

                                # subprocess.Popen(["/usr/local/bin/cnvrt.sh", temp_video_path, vdst], stdout=subprocess.PIPE).wait()

                                r = ""
                                p = subprocess.Popen(["/usr/local/bin/dur.sh", vdst], stdout=subprocess.PIPE)
                                for line in p.stdout:
                                    r += line
                                    p.wait()
                                print p.returncode

                                g.media.duration = int(r.strip())

                                if not g.media.video:
                                    os.symlink(vdst, vdst + ".mp4")

                                g.media.video = os.path.join('/global/upload', media_folder, 'source')
                            if request.values.get('endcard') and request.values.get('endcard') != g.media.endcard:
                                g.media.state = Media.State.NONE

                                g.media.endcard = os.path.join('/global/upload', media_folder, 'endcard')

                                temp_endcard_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                                 *(request.values['endcard'].split('/')[-1:]))
                                vdst = os.path.join(destination_folder, 'endcard')

                                capp.logger.debug("moving [" + temp_endcard_path + "] -> [" + vdst + "]")

                                os.rename(temp_endcard_path, vdst)

                            if request.values.get('overlay') and request.values.get('overlay') != g.media.overlay:
                                g.media.state = Media.State.NONE
                                g.media.overlay = os.path.join('/global/upload', media_folder, 'overlay')

                                temp_overlay_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                                 *(request.values['overlay'].split('/')[-1:]))
                                vdst = os.path.join(destination_folder, 'overlay')

                                capp.logger.debug("moving [" + temp_overlay_path + "] -> [" + vdst + "]")

                                os.rename(temp_overlay_path, vdst)

                            g.media.closable        = form.data.get('closable')
                            g.media.uri_target      = form.data.get('uri_target')
                            g.media.action_end      = form.data.get('action_end')
                    elif g.media.action_click == Media.ClickActions.CALL:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.SMS:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.URL:
                        g.media.uri                 = form.data.get('uri')
                        g.media.uri_target          = form.data.get('uri_target')
                    elif g.media.action_click == Media.ClickActions.NONE:
                        pass
                    else:
                        capp.logger.exception('Invalid click action type')

                    Session.commit()
                    return redirect(redirect_path)
                except:
                    capp.logger.exception('EXCEPTION IN /media/form')
                    return redirect(redirect_exc_path)
        # VIDEO
        if form_type == Media.Type.VIDEO:
            form = MediaVideoForm(request.form, obj=g.media)
            video_form = form

            if request.method == "POST" and form.validate():
                try:
                    g.media.name                = form.data.get('name')
                    g.media.campaign            = g.campaign.id
                    g.media.type                = Media.Type.VIDEO

                    g.media.action_click        = form.data.get('action_click')

                    g.media.action_end          = form.data.get('action_end')
                    g.media.closable            = form.data.get('closable')
                    g.media.daily_limit         = form.data.get('daily_limit')
                    g.media.session_limit       = form.data.get('session_limit')

                    g.media.placement           = Media.Placement.INTERSTITIAL

                    Session.add(g.media)
                    Session.flush()

                    media_folder = '%s/%s/%s' % (request.environ['REMOTE_USER'], campaign_id, g.media.id)
                    destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

                    if not os.path.isdir(destination_folder):
                        os.makedirs(destination_folder)

                    if request.values.get('video') and request.values.get('video') != g.media.video:
                        g.media.state = Media.State.NONE

                        temp_video_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                       *(request.values['video'].split('/')[-1:]))

                        vdst = os.path.join(destination_folder, 'source')

                        move(temp_video_path, vdst)

                        # subprocess.Popen(["/usr/local/bin/cnvrt.sh", temp_video_path, vdst], stdout=subprocess.PIPE).wait()

                        r = ""
                        p = subprocess.Popen(["/usr/local/bin/dur.sh", vdst], stdout=subprocess.PIPE)
                        for line in p.stdout:
                            r += line
                            p.wait()
                        print p.returncode

                        g.media.duration = int(r.strip())

                        if not g.media.video:
                            os.symlink(vdst, vdst + ".mp4")

                        g.media.video = os.path.join('/global/upload', media_folder, 'source')
                    if request.values.get('endcard') and request.values.get('endcard') != g.media.endcard:
                        g.media.state = Media.State.NONE

                        g.media.endcard = os.path.join('/global/upload', media_folder, 'endcard')

                        temp_endcard_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                         *(request.values['endcard'].split('/')[-1:]))
                        vdst = os.path.join(destination_folder, 'endcard')

                        capp.logger.debug("moving [" + temp_endcard_path + "] -> [" + vdst + "]")

                        os.rename(temp_endcard_path, vdst)

                    if request.values.get('overlay') and request.values.get('overlay') != g.media.overlay:
                        g.media.state = Media.State.NONE
                        g.media.overlay = os.path.join('/global/upload', media_folder, 'overlay')

                        temp_overlay_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                         *(request.values['overlay'].split('/')[-1:]))
                        vdst = os.path.join(destination_folder, 'overlay')

                        capp.logger.debug("moving [" + temp_overlay_path + "] -> [" + vdst + "]")

                        os.rename(temp_overlay_path, vdst)

                    if g.media.action_click   == Media.ClickActions.CALL:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.SMS:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.URL:
                        g.media.uri                 = form.data.get('uri')
                        g.media.uri_target          = form.data.get('uri_target')
                    elif g.media.action_click == Media.ClickActions.NONE:
                        pass
                    else:
                        capp.logger.exception('Invalid click action type')

                    Session.commit()
                    return redirect(redirect_path)
                except:
                    capp.logger.exception('EXCEPTION IN /media/form')
                    return redirect(redirect_exc_path)
        if form_type == Media.Type.MRAID:
            form = MediaMraidForm(request.form, obj=g.media)
            mraid_form = form

            if request.method == "POST" and form.validate():
                try:
                    g.media.name = form.name.data
                    g.media.campaign = g.campaign.id
                    g.media.type = Media.Type.MRAID

                    # g.media.banner_size = form.banner_size.data
                    g.media.width = form.width.data
                    g.media.height = form.height.data

                    g.media.daily_limit = form.data.get('daily_limit')
                    g.media.session_limit = form.data.get('session_limit')

                    if (g.media.width, g.media.height) in [Media.BannerSizes.FULLSCREEN_PHONE(), Media.BannerSizes.FULLSCREEN_TABLET()]:
                        g.media.placement = Media.Placement.INTERSTITIAL
                    else:
                        g.media.placement = Media.Placement.BANNER

                    g.media.mraid_type = form.mraid_type.data

                    Session.add(g.media)
                    Session.flush()

                    if g.media.mraid_type == Media.MRAID_Type.FILE:
                        media_folder = '%s/%s/%s' % (request.environ['REMOTE_USER'], campaign_id, g.media.id)
                        destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

                        g.media.mraid_html = ''
                        g.media.mraid_url = ''

                        if not os.path.isdir(destination_folder):
                            os.makedirs(destination_folder)

                        if request.values.get('mraid_file') and request.values.get('mraid_file') != g.media.mraid_file:
                            g.media.state = Media.State.NONE

                            g.media.mraid_file = os.path.join('/global/upload', media_folder, 'mraid')

                            temp_endcard_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                             *(request.values['mraid_file'].split('/')[-1:]))
                            vdst = os.path.join(destination_folder, 'mraid')

                            capp.logger.debug("moving [" + temp_endcard_path + "] -> [" + vdst + "]")

                            os.rename(temp_endcard_path, vdst)
                    elif g.media.mraid_type == Media.MRAID_Type.URL:
                        g.media.mraid_url = form.mraid_url.data
                        g.media.mraid_file = ''
                        g.media.mraid_html = ''
                    elif g.media.mraid_type == Media.MRAID_Type.HTML:
                        g.media.mraid_html = form.mraid_html.data
                        g.media.mraid_url = ''
                        g.media.mraid_file = ''
                    else:
                        capp.logger.exception("Invalid MRAID type")

                    Session.commit()
                    return redirect(redirect_path)
                except:
                    capp.logger.exception('EXCEPTION IN /media/form')
                    return redirect(redirect_exc_path)

        return render_template('/media/form.mako', forms={'banner': banner_form, 'video': video_form, 'mraid': mraid_form}, current_form=form, mraid_order_form=MraidOrderForm())
