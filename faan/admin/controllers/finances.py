# -*- coding: utf-8 -*-
import json
import time
from calendar import timegm
from datetime import datetime
from sqlalchemy import or_
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.meta import Session
from faan.core.model.pub.application import Application
from faan.core.model.security.account import Account
from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source import Source
from faan.admin.forms.sources import SourceForm
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.pub.statistics import PubStat
from faan.x.util.func import try_int
from faan.x.util.timestamp import TimeStamp


@CProtect(IsSigned())
@CProtect(permissions.source)
@Controller('/finances')
class FinancesController(object):

    @Route("/")
    def index(self):
        return render_template('/balance/finances.mako')

    @Route("/ajax/table")
    def ajax_table(self):
        values = request.values.get('values', '')
        start = request.values.get('start', 0)
        count = request.values.get('count', 50)
        search = request.values.get('search')
        timezone = try_int(request.values.get('timezone'), None)
        date_range = request.values.get('dateRange')
        if date_range is not None:
            date_range = map(int, date_range.split('-'))

        objects = []

        self.apply_filters(start=start, count=count, search=search,
                           date_range=date_range, timezone=timezone)

        for pub, adv in zip(g.pub_data, g.adv_data):
            margin = adv.cost - pub.revenue
            row = {
                'date': pub.days,
                'raws': "{0}".format(pub.impressions),
                'revenue': adv.cost,
                'charges': pub.revenue,
                'margin': margin,
                'ecpm': {'margin': margin, 'raws': pub.impressions},
            }
            objects.append(row)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }
        return jsonify(**data)

    def init_filters(self, date_range=None, timezone=None):

        if date_range is not None:
            #ts, te = [d - tz for d, tz in zip(date_range, [timezone] * 2)]
            ts, te = date_range
        else:
            ts = te = TimeStamp.today() - timezone

        # Pre queries
        query_pub = PubStat.PreQuery()\
            .filter(PubStat.ts_spawn >= ts)\
            .filter(PubStat.ts_spawn < te + TimeStamp.DAYSECONDS - 1)

        query_adv = AdvStat.PreQuery()\
            .filter(AdvStat.ts_spawn >= ts)\
            .filter(AdvStat.ts_spawn < te + TimeStamp.DAYSECONDS - 1)

        return query_pub, query_adv

    def apply_filters(self, start=None, count=None, search=None,
                      date_range=None, timezone=None):
        if timezone is None:
            timezone = -time.timezone

        query_pub, query_adv = self.init_filters(date_range, timezone)

        GROUP_DAILY_PUB = ((PubStat.ts_spawn + timezone) -
                           ((PubStat.ts_spawn + timezone) %
                            TimeStamp.DAYSECONDS))

        GROUP_DAILY_ADV = ((AdvStat.ts_spawn + timezone) -
                           ((AdvStat.ts_spawn + timezone) %
                            TimeStamp.DAYSECONDS))

        query_pub = query_pub.add_column(GROUP_DAILY_PUB.label('days'))\
            .group_by(GROUP_DAILY_PUB)\
            .order_by(GROUP_DAILY_PUB.desc())

        query_adv = query_adv.add_column(GROUP_DAILY_ADV.label('days'))\
            .group_by(GROUP_DAILY_ADV)\
            .order_by(GROUP_DAILY_ADV.desc())

        query_total = query_pub

        if start and count:
            query_adv = query_adv.offset(start)
            query_adv = query_adv.limit(count)
            query_pub = query_pub.offset(start)
            query_pub = query_pub.limit(count)

        g.pub_data = query_pub.all()
        g.adv_data = query_adv.all()
        g.total = query_total.count()

