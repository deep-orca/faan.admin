# -*- coding: utf-8 -*-

import time
import json
from decimal import Decimal
from datetime import datetime, timedelta, date
from flask import g, request, redirect, jsonify
from sqlalchemy import or_
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.meta import Session
from faan.core.model.security.account import Account
from faan.core.model.security.account_extra import AccountExtra, \
    AccountExtraCommunication as Communication, \
    AccountPurchasingInfo as Purchase
from faan.core.model.security.partner_group import PartnerGroup
from faan.core.model.security.message import Message
from faan.core.model.security.comment import Comment
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.pub.statistics import PubStat
from faan.core.X.xemail import send_email_approve, send_email_block,\
    send_email_active
from faan.admin.forms.user import UserForm

from faan.admin.controllers.ajax.balance import deposit


def to_decimal(n):
    try:
        return Decimal(n)
    except StandardError:
        return None


def get_total_new_users():
    new_account = Session.query(Account)\
        .filter(Account.state == Account.State.NEW)
    return new_account.count()


class BaseProfileException(Exception):
    pass


@CProtect(IsSigned())
@CProtect(permissions.profiles)
@Controller('/profile')
class PartnersProfileController():

    def _build_dicts(self, search=None):
        """Builds dictionaries."""

        q = Session.query(PartnerGroup)
        if search:
            q = q.filter(PartnerGroup.name.ilike(u'%{0}%'.format(search)))
        g.groups = q.all()

        g.groups_dict = dict([(gr.id, (str(gr.account_type) +
                                       ' - ' + gr.name, u''))
                              for gr in g.groups])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements):
        """Builds pipeline from 'select' elements. The pipeline means that
        elements dependencies each other."""
        pipeline = {}
        store = {'group': {'name': 'group', 'id': 'select_group'}}
        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline

    @Route('/')
    def index(self):
        """Root page of profile tab."""
        return render_template('/profile/index.mako')

    @Route('/<int:profile_id>/_profile_post_delele_account', methods=['POST'])
    def _profile_post_delele_account(self, profile_id):
        account = Account.Key(profile_id)
        extra = AccountExtra.One(AccountExtra.account_id == profile_id)
        Session.delete(extra)
        Session.delete(account)
        Session.commit()

        return redirect(request.values.get('back') or
                        ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_status', methods=['POST'])
    def _profile_post_status(self, profile_id):
        assigned_status = int(request.values.get('assigned_status', 0))

        account = Account.Key(profile_id)
        account.state = assigned_status

        if assigned_status == Account.State.ACTIVE \
                and account.password == 'password':
            password = Account.random_password()
            send_email_approve(account.email, account.email,
                               password)
            account.password = Account.PassHash(password)
        elif assigned_status == Account.State.ACTIVE:
            send_email_active(account.email, account.email)
        elif assigned_status == Account.State.BANNED:
            send_email_block(account.email)
        Session.commit()
        return redirect(request.values.get('back') or
                        ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_group', methods=['POST'])
    def _profile_post_group(self, profile_id):
        assign_group_id = request.values.get('assign_group_id')
        account = Account.Key(profile_id)
        account.partner_group_id = assign_group_id
        Session.commit()
        return redirect(request.values.get('back') or
                ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_settings', methods=['POST'])
    def _profile_post_settings(self, profile_id):
        account, extra = Session.query(Account, AccountExtra)\
            .filter(Account.id == profile_id)\
            .filter(Account.id == AccountExtra.account_id).one()

        account.type = request.values.get('type') or account.type
        account.state = request.values.get('state') or account.state
        account.email = request.values.get('email') or account.email
        account.groups = request.values.get('group') or account.groups
        account.rate = to_decimal(request.values.get('rate'))
        account.request_cost = to_decimal(request.values.get('request_cost'))
        account.overlay_cost = to_decimal(request.values.get('overlay_cost'))
        account.endcard_cost = to_decimal(request.values.get('endcard_cost'))
        account.completion_ratio = to_decimal(
            request.values.get('completion_ratio'))
        extra.name = request.values.get('name') or extra.name
        extra.surname = request.values.get('surname') or extra.surname
        Session.commit()
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_communication', methods=['POST'])
    def _profile_post_communication(self, profile_id):
        extra_id = request.values['account_extra_id']
        coms = Communication.All(Communication.account_extra_id == extra_id)

        for com in coms:
            com.value = request.values.get('communication_%d' % com.id,
                                           com.value)
        Session.commit()
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_add_communication', methods=['POST'])
    def _profile_post_add_communication(self, profile_id):
        com = Communication()
        com.account_extra_id = request.values['extra_id']
        com.type = request.values['type']
        com.value = request.values['value']
        Session.add(com)
        Session.commit()
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_password', methods=['POST'])
    def _profile_post_password(self, profile_id):
        pass0 = request.values['pass0']
        pass1 = request.values['pass1']

        account = Account.Key(profile_id)

        if pass0 == pass1:
            account.password = Account.PassHash(password=pass0)
            Session.commit()
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_purchase', methods=['POST'])
    def _profile_post_purchase(self, profile_id):
        extra_id = request.values['account_extra_id']

        purchases = Purchase.All(Purchase.account_extra_id == extra_id)

        for purchase in purchases:
            purchase.value = request.values.get('purchase_%d' % purchase.id,
                                                purchase.value)
        Session.commit()
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_add_purchase', methods=['POST'])
    def _profile_post_add_purchase(self, profile_id):
        purchase = Purchase()
        purchase.account_extra_id = request.values['extra_id']
        purchase.type = request.values['type']
        purchase.value = request.values['value']
        purchase.state = Purchase.State.NONE
        Session.add(purchase)
        Session.commit()
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/_profile_post_change_balance', methods=['POST'])
    def _profile_post_change_balance(self, profile_id):
        value = Decimal(request.values['value'])
        comment = request.values.get("comment") or u'Ручное изменение баланса'

        account = Account.Key(profile_id)
        deposit(account, request.environ["REMOTE_USER"], profile_id, 0, value,
                comment=comment, ttype=2)
        return redirect(request.values.get('back') or
            ('/profile/%d' % profile_id))

    @Route('/<int:profile_id>/', methods=['GET'])
    def profile(self, profile_id):
        """Shows the particular profile."""
        g.account = Account.Key(profile_id)
        g.extra = AccountExtra.One(AccountExtra.account_id == profile_id)

        if g.account.type == Account.Type.PARTNER:
            g.partner_group = PartnerGroup.Key(g.account.partner_group_id)
            g.partner_groups = PartnerGroup.All(
                PartnerGroup.account_type == g.account.groups)

        month_before = time.mktime((date.fromtimestamp(time.time()) -
                                    timedelta(days=30)).timetuple())
        if g.account.groups == Account.Groups.ADV:
            stat = AdvStat
        elif g.account.groups == Account.Groups.PUB:
            stat = PubStat
        else:
            stat = None

        if stat:
            g.stats = Session.query(stat)\
                .filter(stat.account == g.account.id)\
                .filter(stat.ts_spawn > month_before)\
                .order_by(stat.ts_spawn.desc()).all()
        else:
            g.stats = []

        g.communications = Session.query(Communication)\
            .filter(Communication.account_extra_id == g.extra.id)\
            .order_by(Communication.type).all()

        g.messages = Message.Count(Message.message_consumer_id == g.account.id,
                                   Message.status == Message.Status.NEW)

        g.comments = Session.query(Comment)\
            .filter(Comment.comment_consumer_id == g.account.id)\
            .order_by(Comment.date.desc()).all()

        g.purchases = Session.query(Purchase)\
            .filter(Purchase.account_extra_id == g.extra.id)\
            .order_by(Purchase.type).all()

        return render_template('/profile/particular_profile.mako')

    def _filter_profile(self, _filter=None, start=None, count=None,
                        search=None):
        q = Session.query(Account, PartnerGroup)\
            .filter(Account.partner_group_id == PartnerGroup.id)\
            .order_by(Account.id.desc())

        if g.group and g.group.isdigit():
            q = q.filter(Account.partner_group_id == g.group)

        if _filter and callable(_filter):
                q = _filter(q)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Account.email.ilike(u'%{0}%'.format(search)),
                    PartnerGroup.name.ilike(u'%{0}%'.format(search)),
                    Account.id == search,
                    Account.balance == search,
                ))
            else:
                q = q.filter(or_(
                    Account.email.ilike(u'%{0}%'.format(search)),
                    PartnerGroup.name.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    @Route('/list/')
    def profile_list(self):
        """Shows user list."""
        g.group = request.values.get('group')
        g.tab = request.values.get('tab', 'new')
        elements = ['group']

        if g.group:
            group = PartnerGroup.Key(g.group)
            g.group_name = group.name

        pipeline = self._build_pipeline(elements)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/profile/list.mako')

    @Route('/create_comment/', methods=['GET', 'POST'])
    def create_comment(self):
        """Adds a comment to a particular user."""
        g.consumer_id = request.values.get('consumer_id')

        if request.method == 'POST':
            consumer = g.consumer_id
            text = request.values.get('text')
            back = request.values.get('back', '/profile/list/')

            if not consumer or not text:
                return redirect(back)
            comment = Comment()
            comment.date = datetime.today()
            comment.comment_consumer_id = consumer
            comment.comment_producer_id = request.environ["REMOTE_USER"]
            comment.text = text
            Session.add(comment)
            Session.commit()
            return redirect(back)

        return render_template('/profile/create_comment.mako')

    @Route('/delete_communication/<int:com_id>')
    def delete_communication(self, com_id):
        """Deletes a communication item for a particular user."""
        com = Communication.Key(com_id)
        Session.delete(com)
        Session.commit()
        return redirect(request.values.get('back', '/profile/list/'))

    @Route('/delete_purchase/<int:purchase_id>')
    def purchase_communication(self, purchase_id):
        """Deletes a purchase item for a particular user."""
        purchase = Purchase.Key(purchase_id)
        Session.delete(purchase)
        Session.commit()
        return redirect(request.values.get('back', '/profile/list/'))

    @Route('/active_purchase/<int:extra_id>/<int:purchase_id>')
    def active_purchase(self, purchase_id, extra_id):
        """Activates a purchase item for a particular user."""
        purchases = Purchase.All(Purchase.account_extra_id == extra_id)
        for purchase in purchases:
            if purchase.id == purchase_id:
                purchase.state = Purchase.State.ACTIVE
            else:
                purchase.state = Purchase.State.NONE

        Session.commit()
        return redirect(request.values.get('back', '/profile/list/'))

    @Route('/create/', methods=['GET', 'POST'])
    def create_profile(self):
        """Creates a new user."""
        g.input_error = None

        account = Account.Default()
        form = UserForm(request.form, obj=account)

        if request.method == 'POST' and form.validate():
            account.email = form.email.data
            account.username = form.email.data
            account.type = form.type.data or Account.Type.PARTNER
            account.state = Account.State.ACTIVE
            account.groups = form.group.data or Account.Groups.ADV
            account.balance = form.balance.data or Decimal(0)
            account.password = Account.PassHash(form.password.data)
            account.date_registration = date.fromtimestamp(time.time())

            Session.add(account)
            Session.commit()

            extra = AccountExtra()
            extra.account_id = account.id
            extra.name = form.name.data or ''
            extra.surname = form.surname.data or ''
            extra.company = form.campaign.data or ''
            extra.description = form.description.data or ''

            Session.add(extra)
            Session.commit()

            if form.send_reg_msg.data:
                send_email_approve(account.email, account.email,
                                   form.password.data)
            return redirect('/profile/%d' % account.id)
        return render_template('/profile/create_profile.mako', form=form)

    @Route("/control/<int:account_id>/<int:state>")
    def control_profile(self, account_id=None, state=None):
        """Manages an account. It allows to approve,
        disable the account."""
        g.tab = request.values.get('tab', 'new')
        if account_id and state in (Account.State.ACTIVE,
                                    Account.State.NEW,
                                    Account.State.BANNED):
            account = Account.Key(account_id)

            if state == Account.State.ACTIVE and account.password == 'password':
                password = Account.random_password()
                send_email_approve(account.email, account.email, password)
                account.password = Account.PassHash(password)
            elif state == Account.State.ACTIVE:
                send_email_active(account.email, account.email)
            elif state == Account.State.BANNED:
                send_email_block(account.email)

            account.state = state
            Session.commit()

        redirect_address = '/profile/list?tab=' + g.tab

        return redirect(redirect_address)

    @Route("/ajax/select")
    def ajax_select(self):
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]

        self._build_dicts(search=search)

        obj_list += [{'id': key, 'name': n}
                     for key, (n, _) in g.groups_dict.items()]
        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/table")
    def ajax_table(self):
        g.tab = tab = request.values.get('tab', '')
        g.group = request.values.get('group')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')

        def _filter(tab):
            return {
                'all': lambda x: x.filter(Account.state.in_([
                    Account.State.ACTIVE,
                    Account.State.SUSPENDED,
                ])),
                'new': lambda x: x.filter(Account.state == Account.State.NEW),
                'banned': lambda x: x.filter(Account.state ==
                                             Account.State.BANNED),
            }[tab]

        self._filter_profile(_filter(tab=tab), start=start, count=count,
                             search=search)
        objects = []
        for account, gr in g.data:
            group = {'id': gr.id, 'name': str(gr.account_type) +' - ' + gr.name}
            d = {
                'id': account.id,
                'name': account.email,
                'state': json.dumps({'id': account.state}),
                'balance': json.dumps({'balance': str(account.balance)}),
                'group': json.dumps(group),
                'action': None,
            }
            objects.append(d)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)
