# -*- coding: utf-8 -*-
import json
from sqlalchemy import or_
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.meta import Session
from faan.core.model.security.account import Account
from faan.core.model.security.partner_group import PartnerGroup
from faan.core.X.xemail import send_email_approve, send_email_block, \
    send_email_active


def get_total_new_publishers():
    try:
        new_account = Session.query(Account)\
            .filter(Account.state == Account.State.NEW)\
            .filter(Account.groups == Account.Groups.PUB)
        return new_account.count()
    except Exception:
        return 0


def get_total_new_advertisers():
    try:
        new_account = Session.query(Account)\
            .filter(Account.state == Account.State.NEW)\
            .filter(Account.groups == Account.Groups.ADV)
        return new_account.count()
    except Exception:
        return 0


class PartnersControllerError(Exception):
    """Main exception for Partners controller."""
    pass


@CProtect(IsSigned())
@CProtect(permissions.partners)
@Controller('/partners')
class PartnersController(object):

    def _build_dicts(self, account_group, search=None):
        """Builds dictionaries."""
        q = Session.query(PartnerGroup)\
            .filter(PartnerGroup.account_type == account_group)
        if search:
            q = q.filter(PartnerGroup.name.ilike(u'%{0}%'.format(search)))
        g.groups = q.all()
        g.groups_dict = dict([(gr.id, (gr.name, u'')) for gr in g.groups])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements):
        """Builds pipeline from 'select' elements. The pipeline means that
        elements dependencies each other."""
        pipeline = {}

        store = {
            'group': {
                'name': 'group',
                'header': u'Группа',
                'id': 'select_group',
                'on_change': True,
                'help_string': '',
            },
        }

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline

    @Route('/group/', methods=['GET', 'POST'])
    def group(self):
        """Shows list of the existing groups."""
        g.tab = request.values.get('account_type', 'pub')

        if request.method == 'POST':
            ag = request.values.get('account_type', '')

            group = PartnerGroup()
            group.name = request.values['group_name']
            group.rate = request.values.get('group_rate') or None
            group.request_cost = request.values.get('group_request') or None
            group.overlay_cost = request.values.get('group_overlay') or None
            group.endcard_cost = request.values.get('group_endcard') or None
            group.completion_ratio = request.values.get('group_ration') or None
            if ag == 'pub':
                group.account_type = Account.Groups.PUB
            elif ag == 'adv':
                group.account_type = Account.Groups.ADV
            else:
                raise PartnersControllerError
            Session.add(group)
            Session.commit()
            return redirect('/partners/group/?account_type=' + ag)
        else:
            g.partner_groups = PartnerGroup.All()
            return render_template('/partners/group.mako')

    @Route('/group/delete/<int:id_group>')
    def delete_group(self, id_group=None):
        g.tab = request.values.get('account_type', 'pub')
        if g.tab.isdigit():
            g.tab = (int(g.tab) == Account.Groups.PUB and 'pub') or \
                    (int(g.tab) == Account.Groups.ADV and 'adv')
        if id_group:
            accounts = Account.All(Account.partner_group_id == id_group)
            for (account,) in accounts:
                account.partner_group_id = PartnerGroup.DEFAULT_GROUP
            Session.commit()
            pgroup = PartnerGroup.Key(id_group)
            Session.delete(pgroup)
            Session.commit()

        return redirect('/partners/group/?account_type=' + g.tab)

    @Route('/group/edit/<int:id_group>/', methods=['GET', 'POST'])
    def edit_group(self, id_group):
        """Form for editing and a group."""
        if request.method == 'POST':
            account_type = request.values.get('account')

            group = PartnerGroup.Key(id_group)
            group.name = request.values['name']
            group.rate = request.values.get('rate') or 0
            group.request_cost = request.values.get('request') or 0
            group.overlay_cost = request.values.get('overlay') or 0
            group.endcard_cost = request.values.get('endcard') or 0
            group.completion_ratio = request.values.get('ration') or 0

            Session.commit()

            if int(account_type) == Account.Groups.PUB:
                return redirect('/partners/group/?account_type=pub')
            elif int(account_type) == Account.Groups.ADV:
                return redirect('/partners/group/?account_type=adv')
            else:
                raise PartnersControllerError
        else:
            g.id_group = id_group
            g.group_name = request.values.get('group_name', '')
            g.group_rate = request.values.get('group_rate') or 0
            g.group_request = request.values.get('group_request') or 0
            g.group_overlay = request.values.get('group_overlay') or 0
            g.group_endcard = request.values.get('group_endcard') or 0
            g.group_ration = request.values.get('group_ration') or 0
            g.account_type = request.values.get('account_type', '')
            return render_template('/partners/edit_group.mako')

    def _filter_partners(self, account_group, _filter=None,
                         start=None, count=None, search=None):
        q = Session.query(Account, PartnerGroup)\
            .filter(Account.type == Account.Type.PARTNER)\
            .filter(Account.groups == account_group)\
            .filter(PartnerGroup.id == Account.partner_group_id)\
            .order_by(Account.id.desc())

        if g.group and g.group.isdigit():
            q = q.filter(Account.partner_group_id == g.group)

        if _filter and callable(_filter):
                q = _filter(q)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Account.email.ilike(u'%{0}%'.format(search)),
                    PartnerGroup.name.like(u'%{0}%'.format(search)),
                    Account.id == search,
                    Account.balance == search,
                ))
            else:
                q = q.filter(or_(
                    Account.email.ilike(u'%{0}%'.format(search)),
                    PartnerGroup.name.like(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    @Route('/pub/')
    def show_pub(self):
        """Show table of the all publishers."""
        g.a_group = Account.Groups.PUB
        g.group = request.values.get('group')
        g.tab = request.values.get('tab', 'new')
        elements = ['group']

        if g.group:
            group = PartnerGroup.Key(g.group)
            g.group_name = group.name

        pipeline = self._build_pipeline(elements)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/partners/index.mako')

    @Route('/adv/')
    def show_adv(self):
        """Show table of the all advertisers."""
        g.a_group = Account.Groups.ADV
        g.group = request.values.get('group')
        g.tab = request.values.get('tab', 'new')
        elements = ['group']

        if g.group:
            group = PartnerGroup.Key(g.group)
            g.group_name = group.name

        pipeline = self._build_pipeline(elements)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/partners/index.mako')

    @Route("/control/<int:account_id>/<int:state>")
    def control_partner(self, account_id, state=None):
        """Manages an account. It allows to approve,
        disable the account."""
        g.tab = request.values.get('tab', 'new')
        if state in (Account.State.ACTIVE, Account.State.NEW,
                     Account.State.BANNED):
            account = Account.Key(account_id)

            if state == Account.State.ACTIVE and account.password == 'password':
                password = Account.random_password()
                send_email_approve(account.email, account.email, password)
                account.password = Account.PassHash(password)
            elif state == Account.State.ACTIVE:
                send_email_active(account.email, account.email)
            elif state == Account.State.BANNED:
                send_email_block(account.email)

            account.state = state
            Session.commit()

        return redirect('/partners/' + request.values.get('back', '') +
                        '?tab=' + g.tab)

    @Route("/ajax/partners/select")
    def ajax_partners_select(self):
        account_group = int(request.values.get('account_group', ''))
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]

        self._build_dicts(account_group, search)

        obj_list += [{'id': key, 'name': n}
                     for key, (n, _) in g.groups_dict.items()]
        data = {
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/partners/table")
    def ajax_partners_table(self):
        g.group = request.values.get('group')
        tab = request.values.get('tab', '')
        g.a_group = request.values.get('account_group', '')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')

        def _filter(tab):
            return {
                'all': lambda x: x.filter(Account.state.in_([
                    Account.State.ACTIVE,
                    Account.State.SUSPENDED,
                ])),
                'new': lambda x: x.filter(Account.state == Account.State.NEW),
                'banned': lambda x: x.filter(Account.state ==
                                             Account.State.BANNED),
            }[tab]

        self._filter_partners(g.a_group, _filter(tab=tab), start=start,
                              count=count, search=search)
        objects = []
        for account, gr in g.data:
            d = {
                'id': account.id,
                'name': account.email,
                'state': json.dumps({'id': account.state}),
                'balance': json.dumps({'balance': str(account.balance)}),
                'group': json.dumps({'id': gr.id, 'name': gr.name}),
                'action': None,
            }
            objects.append(d)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)