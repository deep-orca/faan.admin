# -*- coding: utf-8 -*-
import json
import requests
import urlparse
from sqlalchemy import or_
from itertools import chain
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.adv.campaign import Campaign
from faan.core.model.meta import Session
from faan.core.model.category import Category
from faan.core.model.pub.application import Application
from faan.core.model.pub.unit import Unit
from faan.core.model.security.account import Account
from faan.core.model.security.partner_group import PartnerGroup
from faan.web.forms.publisher import ApplicationForm
from faan.x.util import try_int


def process_url(url, keep_params=('CONTENT_ITEM_ID=',)):
    parsed = urlparse.urlsplit(url)
    filtered_query = '&'.join(q_item for q_item in parsed.query.split('&') if q_item.startswith(keep_params))
    return urlparse.urlunsplit(parsed[:3] + (filtered_query,) + parsed[4:])


def get_total_new_app():
    try:
        new_app = Session.query(Application)\
            .filter(Application.state == Application.State.NONE)
        return new_app.count()
    except Exception:
        return 0


class ContentRating(object):
    rating = None

    def __init__(self, rating):
        if rating == "Low Maturity" or rating == "4+" or rating == "9+":
            self.rating = 1
        elif rating == "Medium Maturity" or rating == "12+":
            self.rating = 2
        elif rating == "High Maturity" or rating == "17+":
            self.rating = 3
        else:
            self.rating = 0


class FilterStore(object):

    @staticmethod
    def filter_state(tab):
        return {
            'all': lambda x: x.filter(Application.state.in_([
                Application.State.ACTIVE,
                Application.State.DISABLED,
            ])),
            'new': lambda x: x.filter(Application.state ==
                                      Application.State.NONE),
            'banned': lambda x: x.filter(Application.state ==
                                         Application.State.SUSPENDED),
        }[tab]

    @staticmethod
    def filter_os(os):
        return {
            'all': lambda x: x,
            'ios': lambda x: x.filter(Application.os == Application.Os.IOS),
            'android': lambda x: x.filter(Application.os ==
                                          Application.Os.ANDROID),
            'windows': lambda x: x.filter(Application.os ==
                                          Application.Os.WINPHONE),
            'mobile_site': lambda x: x.filter(
                Application.os == Application.Os.MOBILE_WEB),
        }[os]


@CProtect(IsSigned())
@CProtect(permissions.applications)
@Controller('/app')
class ApplicationController():

    def _build_dicts(self, select=None, search=None):
        """Builds dictionaries."""
        q = Session.query(Account).order_by(Account.id)
        if select == 'account':
            q = q.filter(Account.email.ilike(u'%{0}%'.format(search)))
        g.accounts = q.all()

        q = Session.query(PartnerGroup)
        if select == 'group':
            q = q.filter(PartnerGroup.name.ilike(u'%{0}%'.format(search)))
        g.groups = q.all()

        g.groups_dict = dict([(gr.id, (str(gr.account_type) +
                                       ' - ' + gr.name, u''))
                              for gr in g.groups])
        g.accounts_dict = dict([(account.id, (account.email,
                                              account.partner_group_id))
                                for account in g.accounts])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements, light=False, select=None,
                        search=None):
        """Builds pipeline from 'select' elements. The pipeline means that
        elements dependencies each other."""
        pipeline = {}
        if not light:
            self._build_dicts(select=select, search=search)

        store = {
            'group': {
                'name': 'group',
                'id': 'select_group',
                'dict': None if light else g.groups_dict,
            },
            'account': {
                'name': 'account',
                'id': 'select_account',
                'dict': None if light else g.accounts_dict,
            },
        }

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline

    def _filter_app(self, filters=None, start=None, count=None, search=None):
        q = Session.query(Application, Account)\
            .filter(Account.id == Application.account)\
            .order_by(Application.id.desc())

        if getattr(g, 'group', None) and g.group.isdigit():
            q = q.filter(Account.partner_group_id == g.group)

        if getattr(g, 'account', None) and g.account.isdigit():
            q = q.filter(Account.id == g.account)

        if filters:
            for f in filters:
                q = f(q)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Application.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Campaign.name.ilike(u'%{0}%'.format(search)),
                    Application.id == search,
                ))
            else:
                q = q.filter(or_(
                    Application.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    @Route("/")
    def index(self):
        g.group = request.values.get('group', '')
        g.account = request.values.get('account', '')
        g.tab = request.values.get('tab', 'new')
        elements = ['group', 'account']

        if g.account:
            account = Account.Key(g.account)
            g.account_name = account.email

        if g.group:
            group = PartnerGroup.Key(g.group)
            g.group_name = group.name

        pipeline = self._build_pipeline(elements, light=True)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/application/index.mako')

    @Route("/<int:app_id>/unit/")
    def unit_list(self, app_id):
        g.application = Application.Key(app_id)
        g.units = Session.query(Unit)\
            .filter(Unit.application == app_id)\
            .order_by(Unit.id.desc()).all()
        return render_template('/application/unit_list.mako')

    @Route("/ajax/select")
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]

        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))

        pipeline = self._build_pipeline(pipeline_elements, select=query,
                                        search=search)

        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            select = pipeline.get(name)
            if not select:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id and _id.isdigit():
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/table")
    def ajax_table(self):
        g.tab = tab = request.values.get('tab', '')
        os = request.values.get('os', '')
        values = request.values.get('values', '')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')
        filters = []

        if os:
            filters += [FilterStore.filter_os(os)]
        if tab:
            filters += [FilterStore.filter_state(tab)]

        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            name = 'account' if name in ('advertiser', 'publisher',) else name
            setattr(g, name, _id if _id and _id.isdigit() else None)

        self._filter_app(filters=filters, start=start, count=count,
                         search=search)
        objects = []
        for app, acc in g.data:
            # Determine name for category.
            categories = app.category
            q = Session.query(Category.name)\
                .filter(Category.id.in_(categories))
            application = {
                'id': app.id,
                'name': app.name,
                'store_url': app.store_url,
                'categories': [category for (category,) in q.all()],
            }
            d = {
                'checkbox': False,
                'id': app.id,
                'application': json.dumps(application),
                'state': json.dumps({'id': app.state}),
                'os': app.os,
                'country': u'All',
                'user': json.dumps({'id': acc.id, 'name': acc.email}),
                'age': app.content_rating,
            }
            objects.append(d)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)

    def _disable_app(self, app_id, commit=True):
        Session.query(Application).filter(Application.id == app_id)\
            .update({"state": Application.State.SUSPENDED})
        if commit:
            Session.commit()

    def _active_app(self, app_id, commit=True):
        Session.query(Application).filter(Application.id == app_id)\
            .update({"state": Application.State.ACTIVE})
        if commit:
            Session.commit()

    def _stop_app(self, app_id, commit=True):
        Session.query(Application).filter(Application.id == app_id)\
            .update({"state": Application.State.DISABLED})
        if commit:
            Session.commit()

    def _delete_app(self, app_id, commit=True):
        app = Application.Get(app_id)
        Session.delete(app)
        if commit:
            Session.flush()
            Session.commit()
        pass

    @Route("/active/<int:app_id>")
    def active_app(self, app_id=None):
        g.tab = request.values.get('tab', 'new')
        self._active_app(app_id)
        redirect_address = '/app?tab=' + g.tab
        return redirect(redirect_address)

    @Route("/stop/<int:app_id>")
    def stop_media(self, app_id=None):
        g.tab = request.values.get('tab', 'new')
        self._stop_app(app_id)
        redirect_address = '/app?tab=' + g.tab
        return redirect(redirect_address)

    @Route("/disable/<int:app_id>")
    def disable_app(self, app_id=None):
        g.tab = request.values.get('tab', 'new')
        self._disable_app(app_id)
        redirect_address = '/app?tab=' + g.tab
        return redirect(redirect_address)

    @Route("/group_actions", methods=["POST"])
    def group_actions(self):
        apps = request.values.get('objects_list')
        action = request.values.get('action')

        method = {
            'active': self._active_app,
            'disable': self._disable_app,
            'delete': self._delete_app,
        }.get(action)

        if not method:
            return {}

        for app in list(set(map(int, apps.split(',')))):
            method(app, commit=False)

        Session.flush()
        Session.commit()

        return "{}"

    @Route('/edit/<int:app_id>', methods=['GET', 'POST'])
    def form(self, app_id=None):

        g.application = application = Application.Single(id=app_id) or \
                                      Application.Default()
        tab = request.values.get('tab', 'new')

        form = ApplicationForm(request.form, obj=application)

        if request.method == 'POST' and form.validate():
            application.os = try_int(form.data.get('os'), Application.Os.NONE)
            application.name = form.data.get('name')

            application.category_groups = form.data.get('category_groups')

            if application.os == Application.Os.MOBILE_WEB:
                categories = Session.query(Category).filter(Category.type == 3, Category.id.in_(form.category_groups.data)).all()
                application.category = list(set(chain.from_iterable([c.related_categories for c in categories])))
            else:
                application.category = form.category_groups.data

            application.content_rating = form.data.get('content_rating')
            application.store_url = form.data.get('store_url')
            application.icon_url = form.data.get('icon_url')
            application.target_categories = form.data.get('target_categories')
            application.flags = application.flags or Application.Flag.NONE
            application.type = application.type or Application.Type.REGULAR
            application.state = application.state or Application.State.NONE
            application.account = g.application.account

            try:
                Session.commit()
                Session.flush()
                return redirect('/app/?tab=' + tab)
            except:
                capp.logger.exception("While updating or creating app")

        #g.categories = Category.All()

        return render_template('/application/form.mako', form=form)

    # App creation
    @Route("/ajax/category", methods=["GET"])
    def categories_list(self, app_id=0):
        platform = request.values.get("platform")
        ctype = request.values.get("type")
        obj_list = []

        search = request.values.get("q")

        q = Session.query(Category.name, Category.id)

        if platform:
            q = q.filter(Category.platform == platform)

        if ctype:
            q = q.filter(Category.type == ctype)

        if search:
            q = q.filter(Category.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": z.id, "name": z.name} for z in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/store/search", methods=["GET"])
    def search(self):
        search = unicode.encode(request.values.get("q"), 'utf-8')
        try:
            platform = int(request.values.get("platform"))
        except:
            platform = 0

        lang = request.values.get("lang")

        if platform == 1:
            try:
                url = "https://itunes.apple.com/search?term={0}&media=software".format(search)

                if lang:
                    url += "&country={0}".format(lang.lower())

                r = requests.get(url).json().get("results", [])

                data = {
                    "meta": {},
                    "objects": [
                        {
                            "name": o.get("trackName"),
                            "genres": o.get("genreIds"),
                            "rating": ContentRating(o.get("trackContentRating")).rating,
                            "icon": o.get("artworkUrl60"),
                            "url": o.get("trackViewUrl")
                        } for o in r
                    ]
                }

            except requests.ConnectionError, e:
                data = {"meta": {"error": "ConnectionError"}, "objects": []}

        elif platform == 2:
            try:
                r = requests.get("https://42matters.com/api/1/apps/search.json?access_token=7b97f9896d7b8a909d1eeb4742f8283849a65ec4&limit=10&q={0}".format(search)).json().get("results", [])

                data = {
                    "meta": {},
                    "objects": [
                        {
                            "name": o.get("title"),
                            "genres": [o.get("cat_int")],
                            "rating": ContentRating(o.get("content_rating")).rating,
                            "icon": o.get("icon_72"),
                            "url": process_url(o.get("market_url"), keep_params=('id=',))
                        } for o in r
                    ]
                }

            except requests.ConnectionError, e:
                data = {"meta": {"error": "ConnectionError"}, "objects": []}
        else:
            data = {"meta": {"error": "InvalidParams"}, "objects": []}

        return jsonify(**data)














