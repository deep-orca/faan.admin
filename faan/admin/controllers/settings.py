from faan.admin.forms.settings import MultiplierEditForm
from faan.admin.lib.countrynodes import generateNodes
from faan.admin.controllers import permissions
from faan.core.model.general.geo import GeoCountry, GeoRegion, GeoCity

__author__ = 'limeschnaps'

from flask import g, request, jsonify

from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.core.model.meta import Session
from faan.x.security import CProtect
from faan.admin.lib.security import IsAdmin, IsSigned

from faan.core.model.security import Account


@CProtect(IsSigned())
@CProtect(permissions.settings)
@Controller("/settings")
class SettingsController(object):
    @Route("/")
    def index(self):
        uid = request.environ["REMOTE_USER"]
        g.account = Account.Get(uid)
        g.nodes = generateNodes()
        form = MultiplierEditForm(request.form)
        return render_template('settings/index.mako', form=form)

    @Route("/multiplier", methods=["POST"])
    def edit_multiplier(self):
        form = MultiplierEditForm(request.form)
        if form.validate():
            if form.geo_type.data == 'country':
                geo = GeoCountry.Key(form.geo_id.data)
            elif form.geo_type.data == 'region':
                geo = GeoRegion.Key(form.geo_id.data)
            elif form.geo_type.data == 'city':
                geo = GeoCity.Key(form.geo_id.data)
            else:
                geo = None

            if geo:
                geo.multiplier = form.multiplier.data
                try:
                    Session.add(geo)
                    Session.flush()
                    Session.commit()
                except:
                    capp.logger.exception("While editing multiplier for %s[%d] - %s" % (form.geo_type.data, form.geo_id.data, geo.name))

        if form.errors:
            response = jsonify({'errors': form.errors})
            response.status_code = 400
        else:
            response = jsonify({'result': 'OK'})
        return response