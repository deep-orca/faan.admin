from faan.admin.lib.security import HasType
from faan.core.X.helpers.permissions import Permissions
from faan.core.model.security import Account

permissions = Permissions(
    index=HasType(Account.Type.MANAGER, Account.Type.DEVELOPER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    applications=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    balance=HasType(Account.Type.ADMINISTRATOR, Account.Type.GOD),
    media=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    news=HasType(Account.Type.ADMINISTRATOR, Account.Type.GOD),
    partners=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    profiles=HasType(Account.Type.ADMINISTRATOR, Account.Type.GOD),
    sdk=HasType(Account.Type.DEVELOPER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    settings=HasType(Account.Type.ADMINISTRATOR, Account.Type.GOD),
    stats=HasType(Account.Type.ADMINISTRATOR, Account.Type.GOD),
    support=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    unit=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    source=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
    campaigns=HasType(Account.Type.MANAGER, Account.Type.ADMINISTRATOR, Account.Type.GOD),
)