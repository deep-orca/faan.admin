# coding=utf-8
import datetime
import time

from flask import request, g, redirect
from sqlalchemy.sql.functions import count
from faan.admin.controllers import permissions

from faan.core.X.xflask import Controller, Route, capp
from faan.core.model.ticket import Ticket, TicketReply
from faan.core.model.meta import Session
from faan.core.X.mako import render_template
from faan.web.app import account
from faan.web.forms.ticket import TicketReplyForm
from faan.admin.lib.security import IsSigned
from faan.x.security import CProtect


__author__ = 'limeschnaps'


def get_total_new_replies():
    new_tickets = Session\
        .query(Ticket.id)\
        .filter(Ticket.state == Ticket.State.OPEN, Ticket.ts_viewed_support == 0)\
        .count()

    result = dict(Session
                  .query(Ticket.state, count(TicketReply.id))
                  .join(TicketReply)
                  .filter(
                      TicketReply.account_id != account.id,
                      Ticket.ts_viewed_support < TicketReply.ts_created
                  )
                  .group_by(Ticket.state).all())

    result.update({Ticket.State.OPEN: new_tickets})
    return result


def get_new_replies(tickets):
    id_list = [t.id for t in tickets]
    result = dict(Session
                  .query(Ticket.id, count(TicketReply.id)).join(TicketReply).filter(
                      Ticket.id.in_(id_list),
                      TicketReply.account_id != account.id,
                      Ticket.ts_viewed_support < TicketReply.ts_created
                  ).group_by(Ticket.id).all())
    result.update({t.id: 1 if t.ts_viewed_support == 0 else 0 for t in tickets})
    return result


@CProtect(IsSigned())
@CProtect(permissions.support)
@Controller("/support")
class SupportController(object):
    @Route("/")
    @Route("/<string:view_type>")
    def list_ticket_view(self, view_type="open"):
        """Shows a list of tickets depending on view type

        template_name :: support/list.mako

        view_type -- [open|resolved] (default `open`)
        """
        print request

        tickets = Session().query(Ticket)
        if view_type == "open":
            tickets = tickets.filter(Ticket.state == Ticket.State.OPEN)
        elif view_type == "resolved":
            tickets = tickets.filter(Ticket.state == Ticket.State.RESOLVED)

        tickets = tickets.all()

        tickets.sort(key=lambda x: (-(1 << x.new_replies_admin[0] * 2 ** 2 << x.urgency), -x.id))

        # Setting context
        g.total_new_replies = get_total_new_replies()
        g.new_replies = get_new_replies(tickets)
        g.tickets = tickets
        g.view_type = view_type

        return render_template("support/list.mako")

    @Route("/<int:ticket_id>", methods=["GET", "POST"])
    def detail_ticket_view(self, ticket_id=None):
        """Showing selected ticket
        Raises `NotOwnedException` if opened by not owner user

        template_name :: support/details.mako
        form_class :: TicketReplyForm
        """
        print request

        ticket = Ticket.Single(id=ticket_id)

        if not ticket:
            raise KeyError

        ticket.ts_viewed_support = int(time.mktime(datetime.datetime.utcnow().timetuple()))  # Viewed by support now
        try:
            Session.add(ticket)
            Session.flush()
            Session.commit()
        except:
            Session.rollback()
            capp.logger.exception("While marking a reply as read")

        form = TicketReplyForm()

        if request.method == "POST" and form.validate():
            reply = TicketReply.Default()
            form.populate_obj(reply)
            reply.ts_created = int(time.mktime(datetime.datetime.utcnow().timetuple()))
            reply.account_id = account.id
            reply.ticket = ticket_id

            try:
                Session.add(reply)
                Session.flush()
                Session.commit()
            except:
                Session.rollback()
                capp.logger.exception("While saving new reply")

        # Setting context
        g.ticket = ticket
        # g.total_new_replies = 0
        g.total_new_replies = get_total_new_replies()
        g.view_type = "open" if ticket.state == Ticket.State.OPEN else "resolved"

        return render_template("support/details.mako", form=form)

    @Route("/<int:ticket_id>/resolve")
    def resolve_ticket_view(self, ticket_id=None):
        """Marking ticket as resolved

        ticket_id -- Ticket ID (default None)
        """
        ticket = Ticket.Single(id=ticket_id)

        if not ticket:
            raise KeyError

        ticket.state = Ticket.State.RESOLVED

        try:
            Session.add(ticket)
            Session.flush()
            Session.commit()
        except:
            Session.rollback()
            capp.logger.exception("While resolving ticket")
            return redirect("/support/%s" % ticket_id)

        return redirect("/support")

