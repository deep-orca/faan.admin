# -*- coding: utf-8 -*-
import json
from sqlalchemy import or_
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.meta import Session
from faan.core.model.pub.application import Application
from faan.core.model.security.account import Account
from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source import Source
from faan.admin.forms.sources import SourceForm
from faan.admin.forms.units import UnitForm
from faan.x.util import try_int


class Tabs(object):
    NEW = 1
    ACTIVE = 2
    BANNED = 3

    @classmethod
    def tabs(cls):
        return cls.NEW, cls.ACTIVE, cls.BANNED


@CProtect(IsSigned())
@CProtect(permissions.unit)
@Controller('/unit')
class UnitController(object):

    @Route("/")
    def index(self):
        g.account = request.values.get('account', '')
        g.application = request.values.get('application', '')
        g.tab = try_int(request.values.get('tab'), Tabs.ACTIVE)
        elements = ['account', 'application']

        if g.account:
            account = Account.Key(g.account)
            g.account_name = account.email

        if g.application:
            app = Application.Key(g.application)
            g.application_name = app.name

        pipeline = self._build_pipeline(elements, light=True)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/unit/index.mako')

    @Route('/<int:app_id>/new', methods=['GET', 'POST'])
    @Route('/<int:app_id>/<int:unit_id>', methods=["GET", "POST"])
    def form(self, app_id, unit_id=None):
        g.app = Application.Key(app_id)
        g.unit = unit = Unit.Single(id=unit_id) or Unit.Default()

        form = UnitForm(request.form, obj=unit)

        if request.method == 'POST' and form.validate():
            unit.application = g.app.id
            unit.name = form.name.data
            unit.mod_factor = form.mod_factor.data
            unit.width = form.width.data
            unit.height = form.height.data
            unit.type = form.type.data
            unit.session_limit = form.session_limit.data
            unit.daily_limit = form.daily_limit.data

            if form.type.data == Unit.Type.INTERSTITIAL:
                unit.refresh_rate = 0
            elif form.type.data == Unit.Type.BANNER:
                unit.refresh_rate = form.refresh_rate.data

            unit.flags = unit.flags or Unit.Flag.NONE
            unit.state = unit.state or Unit.State.ACTIVE
            try:
                Session.add(unit)
                Session.flush()
                Session.commit()

                return redirect('/app/{0}/unit'.format(g.app.id))
            except:
                capp.logger.exception('EXCEPTION IN /app/unit/form')

        return render_template('/unit/form.mako', form=form)

    @Route('/<int:unit_id>/source', methods=["GET", "POST"])
    def source_list(self, unit_id):
        """Source list for a specific unit."""
        g.available_source_types = []
        g.unit = Unit.Key(unit_id)
        g.sources = Session.query(Source)\
            .filter(Source.unit == unit_id)\
            .order_by(Source.id.desc()).all()
        # Get available types for new sources of this unit.
        unavailable = Session.query(Source.type)\
            .filter(Source.unit == unit_id).all()
        available_source_types_id = set([t[0] for t in SourceForm.Types]) - \
                                    set([t[0] for t in unavailable])
        for _type in SourceForm.Types:
            if _type[0] in available_source_types_id:
                g.available_source_types.append(_type)
        return render_template('/unit/source_list.mako')


    @Route("/ajax/select")
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]
        unit_filters = ['unit_type', 'width', 'height']

        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))
        pipeline_elements = [pe for pe in pipeline_elements
                             if pe not in unit_filters]

        pipeline = self._build_pipeline(pipeline_elements, select=query,
                                        search=search)
        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            select = pipeline.get(name)
            if not select:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id and _id.isdigit():
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]
        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/table")
    def ajax_table(self):
        g.tab = try_int(request.values.get('tab'), Tabs.ACTIVE)
        values = request.values.get('values', '')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')

        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            name = 'account' if name in ('advertiser', 'publisher',) else name
            setattr(g, name, _id if _id and _id.isdigit() else None)

        self.apply_filters(start=start, count=count, search=search)

        objects = []
        for unit, app, acc in g.data:
            dunit = {
                'id': unit.id,
                'name': unit.name,
                'size': "{0}x{1}".format(unit.width, unit.height),
                'app_id': app.id,
                'app_name': app.name,
                'acc_id': acc.id,
                'acc_email': acc.email,
            }
            d = {
                'checkbox': None,
                'unit': json.dumps(dunit),
                'unit_type': unit.type,
                'state': unit.state,
                'action': None,
            }
            objects.append(d)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)

    @Route("/<int:app_id>/<int:unit_id>/state/<int:state>")
    def state(self, app_id, unit_id, state):
        obj = Unit.Key(unit_id)
        obj.state = state

        try:
            Session.add(obj)
            Session.flush()
            Session.commit()
        except StandardError:
            capp.logger.exception("While setting unit state")

        return redirect("/app/{0}/unit/".format(app_id))

    @Route("/<int:app_id>/<int:unit_id>/delete")
    def delete(self, app_id, unit_id):
        obj = Unit.Key(unit_id)

        try:
            Session.delete(obj)
            Session.flush()
            Session.commit()
        except StandardError:
            capp.logger.exception("While deleting unit")

        return redirect("/app/{0}/unit/".format(app_id))

    @Route("/actions", methods=["POST"])
    def group_actions(self):
        units = request.values.get('objects_list')
        action = request.values.get('action')

        method = {
            'active': self.active_unit,
            'disable': self.disable_unit,
            'delete': self.delete_unit,
        }.get(action)

        if not method:
            return "{}"

        for m in list(set(units.split(','))):
            method(m, commit=False)

        Session.flush()
        Session.commit()

        return "{}"
    
    def disable_unit(self, unit_id, commit=True):
        Session.query(Unit).filter(Unit.id == unit_id)\
            .update({"state": Unit.State.DISABLED})
        if commit:
            Session.commit()

    def active_unit(self, unit_id, commit=True):
        Session.query(Unit).filter(Unit.id == unit_id)\
            .update({"state": Unit.State.ACTIVE})
        if commit:
            Session.commit()

    def delete_unit(self, unit_id, commit=True):
        m = Unit.Key(unit_id)
        Session.delete(m)
        if commit:
            Session.flush()
            Session.commit()

    def apply_filters(self, start=None, count=None, search=None):
        q = Session.query(Unit, Application, Account)\
            .filter(Account.id == Application.account)\
            .filter(Application.id == Unit.application)\
            .order_by(Unit.id.desc())

        if getattr(g, 'account', None) and g.account.isdigit():
            q = q.filter(Account.id == g.account)

        if getattr(g, 'application', None) and g.application.isdigit():
            q = q.filter(Application.id == g.application)

        if getattr(g, 'tab', None):
            q = {
                Tabs.NEW: q.filter(Unit.state == Unit.State.NONE),
                Tabs.ACTIVE: q.filter(Unit.state == Unit.State.ACTIVE),
                Tabs.BANNED: q.filter(Unit.state == Unit.State.DISABLED),
            }.get(g.tab)

        if getattr(g, 'unit_type', None) and g.unit_type is not None:
            q = q.filter(Unit.type == g.unit_type)

        if getattr(g, 'width', None) and g.width is not None \
                and g.height is not None:
            q = q.filter(Unit.width == g.width, Unit.height == g.height)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Application.name.ilike(u'%{0}%'.format(search)),
                    Unit.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Unit.id == search,
                ))
            else:
                q = q.filter(or_(
                    Application.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Unit.name.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    def _build_dicts(self, select=None, search=None):
        """Builds dictionaries."""
        q = Session.query(Account).order_by(Account.id)
        if select == 'account':
            q = q.filter(Account.email.ilike(u'%{0}%'.format(search)))
        g.accounts = q.all()

        q = Session.query(Application)
        if select == 'application':
            q = q.filter(Application.name.ilike(u'%{0}%'.format(search)))
        g.applications = q.all()

        g.accounts_dict = dict([(account.id, (account.email, u''))
                               for account in g.accounts])
        g.applications_dict = dict([(app.id, (app.name, app.account))
                                   for app in g.applications])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements, light=False, select=None,
                        search=None):
        pipeline = {}
        if not light:
            self._build_dicts(select=select, search=search)

        store = {
            'account': {
                'name': 'account',
                'id': 'select_account',
                'dict': None if light else g.accounts_dict,
            },
            'application': {
                'name': 'application',
                'id': 'select_application',
                'dict': None if light else g.applications_dict,
            },
        }

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline