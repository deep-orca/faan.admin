# -*- coding: utf-8 -*-
from flask import request, redirect
from faan.core.X.xflask import Controller, Error
from faan.core.X.mako import render_template
from faan.core.model.security import Account


@Controller
class ErrorController():
    @Error(404)
    def page_404(self, e):
        if request.environ.get('REMOTE_USER'):
            return render_template('/error/404_loged.mako'), 404
        else:
            return render_template('/error/404.mako'), 404

    @Error(403)
    def page_403(self, e):
        if request.environ.get('REMOTE_USER'):
            account = Account.Get(request.environ.get('REMOTE_USER'))
            if account.type not in [Account.Type.PARTNER, Account.Type.USER]:
                return render_template('/error/403_logged.mako'), 403
        return redirect("/sign/in")

