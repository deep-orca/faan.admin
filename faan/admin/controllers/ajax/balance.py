# -*- coding: utf-8 -*-

from flask import g, request, redirect, make_response, jsonify
from sqlalchemy.sql.expression import desc
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.util._collections import KeyedTuple
from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.core.model.security import Account
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model import Category
from faan.core.model.meta import Session
from faan.core.model.transaction import Transaction
import decimal
import time
import operator
from datetime import datetime
from faan.x.util import try_int


class SAJsonSerializer(object):
    def __init__(self):
        pass

    @classmethod
    def serialize(cls, sa_query, sort=None, ts_fields=None, tz=0):
        """
        Decimal        -> Float
        Unix timestamp -> JS timestamp

        Returns JSON serializeable dict
        """
        if not ts_fields:
            ts_fields = []

        result = []
        r = {}

        def _cquery(q):
            q_obj = q.__dict__
            #print q.__dict__
            for l in q_obj.get("_labels", None) or q_obj:
                o = q_obj[l]
                #print o[0], type(o[0])
                if not isinstance(o, KeyedTuple):
                    _obj = o.__dict__
                    try:
                        del _obj["_sa_instance_state"]
                    except KeyError:
                        pass
                    #_obj = {"{0}_{1}".format(l.lower(), k): v for k, v in _obj.iteritems()}
                    #for k,v in _obj.iteritems():
                    #    _obj_copy.update({l.lower() + "_" + k: v })
                    #r.update(_obj)
                else:
                    _obj = dict(zip(o.keys(), o))
                    #_obj = {"{0}_{1}".format(l.lower(), k): v for k, v in _obj.iteritems()}
                    #r.update()

                _obj = {"{0}_{1}".format(l.lower(), k): v for k, v in _obj.iteritems()}
                r.update(_obj)

        for o in sa_query:
            r = {}
            _cquery(o)
            result.append(r)

        result = [{k: float(v) if isinstance(v, decimal.Decimal) else v if k not in ts_fields else int("%s000" % (v - tz)) for k, v in o.iteritems()} for o in result]
        if sort:
            sort_field, sort_order = sort
            result = sorted(result, key=operator.itemgetter(sort_field))
            if sort_order == -1:
                result.reverse()

        return result


def _deposit(account, issuer, dst, psid, amount, comment="", reference=0, wallet="", ttype=0):
    """
    Deposits money for current user
    """
    if account:
        t = Transaction.Default()
        t.amount = float(amount)
        t.dst_acc = dst
        t.comment = comment
        t.issuer_acc = issuer
        t.ps = psid
        t.wallet = wallet
        t.reference = reference
        t.ts_spawn = time.mktime(datetime.utcnow().timetuple())
        t.type = ttype

        try:
            Session.add(t)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("EXCEPTION IN deposit ON transaction")
            return jsonify({"result": {"state": "error"}})

        account.balance += t.amount

        try:
            Session.add(account)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("EXCEPTION IN deposit ON account")
            return jsonify({"result": {"state": "error"}})

        data = {
            "result": {"state": "ok"}
        }
    else:
        return jsonify({"result": {"state": "error"}})
    return jsonify(**data)

deposit = _deposit


@CProtect(IsSigned())
@Controller("/ajax/balance")
class AjaxController(object):
    @Route("/get")
    def get(self):
        """
        Returns balance of current user
        """
        uid = request.environ["REMOTE_USER"]
        try:
            account = Session().query(Account).filter(Account.id == uid).one()
        except MultipleResultsFound, e:
            return jsonify({"result": {"state": "error"}})

        balance = account.balance

        data = {
            "result": {
                "state": "ok",
                "balance": balance
            }
        }

        return jsonify(**data)

    @Route("/callback/<int:psid>")
    def callback(self, psid=0):
        """
        Recieving callback from pay system
        Processing callback and calling `_deposit`
        """
        # TODO: Process callback in a proper way (depends on pay system)

        return deposit(40, 40, psid, 150, comment="Deposit from {0} PS".format(psid), ttype=2)


    @Route("/list")
    def list_transactions(self):
        """
        Returns a list of transactions of current user
        """
        #uid = request.environ["REMOTE_USER"]
        ltype = try_int(request.values.get("type"), -1)
        atype = request.values.get("atype")
        agroup = request.values.get("agroup")
        uid = request.values.get("uid")

        objects = []

        if ltype is not None:
            objects = Session.query(Transaction, Account)\
                .filter(Transaction.type != Transaction.Type.IMPRESSION)
            if ltype == 0:
                objects = objects.filter(Transaction.dst_acc == Account.id, Transaction.amount >= 0)
            elif ltype == 1:
                objects = objects.filter(Transaction.dst_acc == Account.id, Transaction.amount < 0)
            else:
                objects = objects.filter(Transaction.dst_acc == Account.id)\

            if atype is not None and atype != "":
                objects = objects.filter(Account.type == atype)

            if agroup is not None and agroup != "":
                objects = objects.filter(Account.groups == agroup)

            if uid is not None and uid != "":
                objects = objects.filter(Account.id == uid)

            objects = objects.all()

        sobjects = SAJsonSerializer.serialize(objects, ts_fields=["transaction_ts_spawn", ], sort=("transaction_ts_spawn", -1))

        acc_info = {a.id: a.username for a in Session.query(Account.username, Account.id).all()}
        acc_info.update({0: u"Система"})

        for o in sobjects:
            o.update({
                "dst_acc_name": acc_info.get(o.get("transaction_dst_acc")),
                "issuer_acc_name": acc_info.get(o.get("transaction_issuer_acc"))
            })

        data = {
            "meta": {},
            "objects": sobjects
        }
        return jsonify(**data)