# -*- coding: utf-8 -*-

import decimal
import operator

from flask import request, jsonify
from sqlalchemy.util._collections import KeyedTuple

from faan.core.X.xflask import Controller, Route
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned
from faan.core.model.security import Account
from faan.core.model.meta import Session


class SAJsonSerializer(object):
    def __init__(self):
        pass

    @classmethod
    def serialize(cls, sa_query, sort=None, ts_fields=None, tz=0):
        """
        Decimal        -> Float
        Unix timestamp -> JS timestamp

        Returns JSON serializeable dict
        """
        if not ts_fields:
            ts_fields = []

        result = []

        def _cquery(q):
            r = {}
            q_obj = q.__dict__
            try:
                for l in q_obj["_labels"]:
                    o = q_obj[l]
                    if not isinstance(o, KeyedTuple):
                        _obj = o.__dict__
                        try:
                            del _obj["_sa_instance_state"]
                        except KeyError:
                            pass
                    else:
                        _obj = dict(zip(o.keys(), o))

                    _obj = {"{0}_{1}".format(l.lower(), k): v for k, v in _obj.iteritems()}
                    r.update(_obj)
            except KeyError:
                o = q
                if not isinstance(o, KeyedTuple):
                    _obj = o.__dict__
                    try:
                        del _obj["_sa_instance_state"]
                    except KeyError:
                        pass
                else:
                    _obj = dict(zip(o.keys(), o))
                r = _obj

            return r

        for o in sa_query:
            result.append(_cquery(o))

        result = [
            {k: float(v) if isinstance(v, decimal.Decimal) else v if k not in ts_fields else int("%s000" % (v - tz)) for
             k, v in o.iteritems()} for o in result]
        if sort:
            sort_field, sort_order = sort
            result = sorted(result, key=operator.itemgetter(sort_field))
            if sort_order == -1:
                result.reverse()

        return result


@CProtect(IsSigned())
@Controller("/ajax/accounts")
class AjaxController(object):
    @Route("/list")
    def list_accounts(self):
        """
        Returns a list of accounts
        """

        _filter = {}
        for k, v in request.values.iteritems():
            if k.startswith("filter_"):
                try:
                    if float(v).is_integer():
                        v = int(v)
                    else:
                        v = float(v)
                except ValueError:
                    pass

                _filter.update({k.replace("filter_", ""): v})

        objects = Session.query(Account)

        if _filter.get("groups"):
            objects = objects.filter(Account.groups == _filter.get("groups"))

        if _filter.get("type"):
            objects = objects.filter(Account.type == _filter.get("type"))

        objects = objects.all()

        sobjects = SAJsonSerializer.serialize(objects)
        data = {
            "meta": {},
            "objects": sobjects
        }
        return jsonify(**data)