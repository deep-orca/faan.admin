# -*- coding: utf-8 -*-

import time
from calendar import timegm
from datetime import datetime
from flask import g, request, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.x.util.func import try_int
from faan.x.util.timestamp import TimeStamp
from faan.core.model.meta import Session
from faan.core.model.security.account import Account
from faan.core.model.pub.application import Application
from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source import Source
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model.security.partner_group import PartnerGroup
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.pub.statistics import PubStat
from faan.core.model.general.geo import GeoCountry, GeoRegion, GeoCity


class AdminStatError(Exception):
    """Main error for admin Statistics class."""
    pass


class FilterStore(object):

    @staticmethod
    def first_column(gr, page, timezone):
        stat = PubStat if gr == Account.Groups.PUB else AdvStat
        GROUP_DAILY = ((stat.ts_spawn + timezone) -
                       (stat.ts_spawn + timezone) % (TimeStamp.DAYSECONDS))
        return {
            'days': lambda x: x.add_column(GROUP_DAILY.label(page))
                               .group_by(GROUP_DAILY),
            'hours': lambda x: x.add_column(stat.Presets.GROUP_HOURLY
                                            .label(page))
                                .group_by(stat.Presets.GROUP_HOURLY),
            'acc': lambda x: x.add_column(stat.account.label(page))
                                .group_by(stat.account),
            'cam': lambda x: x.add_column(stat.campaign.label(page))
                                .group_by(stat.campaign),
            'media': lambda x: x.add_column(stat.media.label(page))
                                .group_by(stat.media),
            'app': lambda x: x.add_column(stat.application.label(page))
                                .group_by(stat.application),
            'unit': lambda x: x.add_column(stat.unit.label(page))
                                .group_by(stat.unit),
            'source': lambda x: x.add_column(stat.source.label(page))
                                .group_by(stat.source),
            'country': lambda x: x.add_column(stat.country.label(page))
                                .group_by(stat.country),
            'region': lambda x: x.add_column(stat.region.label(page))
                                .group_by(stat.region),
            'city': lambda x: x.add_column(stat.city.label(page))
                                .group_by(stat.city),
        }.get(page)


@CProtect(IsSigned())
@CProtect(permissions.stats)
@Controller('/stats')
class Statistics():

    def _build_dicts(self, _account_group, select=None, search=None):
        """Builds dictionaries."""
        q = Session.query(Account)\
            .filter(Account.groups == _account_group)\
            .order_by(Account.id)
        if select in ('account', 'publisher', 'advertiser',):
            q = q.filter(Account.username.ilike(u'%{0}%'.format(search)))
        g.accounts = q.all()

        q = Session.query(PartnerGroup)\
            .filter(PartnerGroup.account_type == _account_group)
        if select == 'group':
            q = q.filter(PartnerGroup.name.ilike(u'%{0}%'.format(search)))
        g.groups = q.all()

        if _account_group == Account.Groups.PUB:
            q = Session.query(Application)
            if select == 'application':
                q = q.filter(Application.name.ilike(u'%{0}%'.format(search)))
            g.applications = q.all()
            q = Session.query(Unit)
            if select == 'unit':
                q = q.filter(Unit.name.ilike(u'%{0}%'.format(search)))
            g.units = q.all()
            q = Session.query(Source.id, Source.name, Source.unit)
            if select == 'source':
                q = q.filter(Source.name.ilike(u'%{0}%'.format(search)))
            g.sources = q.all()
        elif _account_group == Account.Groups.ADV:
            q = Session.query(Campaign)
            if select == 'campaign':
                q = q.filter(Campaign.name.ilike(u'%{0}%'.format(search)))
            g.campaigns = q.all()
            q = Session.query(Media)
            if select == 'media':
                q = q.filter(Media.name.ilike(u'%{0}%'.format(search)))
            g.medias = q.all()
        else:
            raise AdminStatError

        q = Session.query(GeoCountry)
        if select == 'country':
            q = q.filter(GeoCountry.name.ilike(u'%{0}%'.format(search)))
        g.countrys = q.all()

        q = Session.query(GeoRegion)
        if select == 'region':
            q = q.filter(GeoRegion.name.ilike(u'%{0}%'.format(search)))
        g.regions = q.all()

        q = Session.query(GeoCity)
        if select == 'city':
            q = q.filter(GeoCity.name.ilike(u'%{0}%'.format(search)))
        g.citys = q.all()

        g.groups_dict = dict([(gr.id, (gr.name, u'')) for gr in g.groups])
        g.accounts_dict = dict([(account.id, (account.username,
                                              account.partner_group_id))
                                for account in g.accounts])

        g.countrys_dict = dict([(c.id, (c.name, u'')) for c in g.countrys])
        g.regions_dict = dict([(r.id, (r.name, r.country)) for r in g.regions])
        g.citys_dict = dict([(c.id, (c.name, c.region)) for c in g.citys])

        if _account_group == Account.Groups.PUB:
            g.applications_dict = dict([(app.id, (app.name, app.account))
                                        for app in g.applications])
            g.units_dict = dict([(unit.id, (unit.name, unit.application))
                                for unit in g.units])
            g.sources_dict = dict([(s.id, (s.name, s.unit))
                                   for s in g.sources])
        elif _account_group == Account.Groups.ADV:
            g.campaigns_dict = dict([(c.id, (c.name, c.account))
                                     for c in g.campaigns])
            g.medias_dict = dict([(m.id, (m.name, m.campaign))
                                  for m in g.medias])
        else:
            raise AdminStatError

    def _build_pipeline(self, pipeline_elements, account_group, select=None,
                        search=None, light=False):
        """Builds pipeline from 'select' elements. The pipeline means that
        elements dependencies each other."""
        pipeline = {}
        if not light:
            self._build_dicts(account_group, select=select, search=search)

        store = {
            'group': {
                'name': 'group', 'dict': None if light else g.groups_dict,
            },
            'country': {
                'name': 'country', 'dict': None if light else g.countrys_dict,
            },
            'region': {
                'name': 'region', 'dict': None if light else g.regions_dict,
            },
            'city': {
                'name': 'city', 'dict': None if light else g.citys_dict,
            },
        }

        if account_group == Account.Groups.PUB:
            store.update({
                'publisher': {
                    'name': 'publisher',
                    'dict': None if light else g.accounts_dict,
                },
                'application': {
                    'name': 'application',
                    'dict': None if light else g.applications_dict,
                },
                'unit': {
                    'name': 'unit', 'dict': None if light else g.units_dict
                },
                'source': {
                    'name': 'source', 'dict': None if light else g.sources_dict
                },
            })
        elif account_group == Account.Groups.ADV:
            store.update({
                'advertiser': {
                    'name': 'advertiser',
                    'dict': None if light else g.accounts_dict,
                },
                'campaign': {
                    'name': 'campaign',
                    'dict': None if light else g.campaigns_dict,
                },
                'media': {
                    'name': 'media', 'dict': None if light else g.medias_dict,
                },
            })
        else:
            raise AdminStatError

        for v in store.values():
            v.update({'id': 'select_' + v['name']})

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline

    def _apply_filters(self, account_group, timezone, page):
        if account_group == Account.Groups.PUB:
            q = self._init_filters(Account.Groups.PUB, timezone=timezone)
        elif account_group == Account.Groups.ADV:
            q = self._init_filters(Account.Groups.ADV, timezone=timezone)
        else:
            raise AdminStatError

        _filter = FilterStore.first_column(account_group, page, timezone)
        q = _filter(q)

        if account_group == Account.Groups.PUB:
            Stat = PubStat
        elif account_group == Account.Groups.ADV:
            Stat = AdvStat
        else:
            raise AdminStatError

        if getattr(g, 'group', None):
            accounts = Account.All(Account.partner_group_id == g.group)
            allowed_accounts = [acc.id for acc in accounts]
            if allowed_accounts:
                q = q.filter(Stat.account.in_(allowed_accounts))
            else:
                g.stats = []

        if getattr(g, 'account', None):
            q = q.filter(Stat.account == int(g.account))

        if account_group == Account.Groups.PUB and getattr(g, 'application',
                                                           None):
            q = q.filter(PubStat.application == g.application)

        if account_group == Account.Groups.PUB and getattr(g, 'unit', None):
            q = q.filter(PubStat.unit == g.unit)

        if account_group == Account.Groups.PUB and getattr(g, 'source', None):
            q = q.filter(PubStat.source == g.source)

        if account_group == Account.Groups.ADV and getattr(g, 'campaign', None):
            q = q.filter(AdvStat.campaign == g.campaign)

        if account_group == Account.Groups.ADV and getattr(g, 'media', None):
            q = q.filter(AdvStat.media == g.media)

        if getattr(g, 'country', None):
            q = q.filter(Stat.country == g.country)

        if getattr(g, 'region', None):
            q = q.filter(Stat.region == g.region)

        if getattr(g, 'city', None):
            q = q.filter(Stat.city == g.city)

        if not hasattr(g, 'stats'):
            g.stats = q.all()

    def _init_filters(self, account_group, timezone=None):
        if timezone is None:
            timezone = -time.timezone
        try:
            g.date_period = request.values['date']
            g.ts, g.te = request.values['date'].split(' - ')
            g.ts = timegm(datetime.strptime(g.ts, "%d.%m.%Y").timetuple())
            g.te = timegm(datetime.strptime(g.te, "%d.%m.%Y").timetuple())
            g.ts, g.te = g.ts - timezone, g.te - timezone
        except StandardError:
            g.ts = g.te = TimeStamp.today() - timezone
            today = datetime.utcfromtimestamp(g.ts).strftime('%d.%m.%Y')
            g.date_period = today + ' - ' + today

        if account_group == Account.Groups.PUB:
            q = PubStat.PreQuery()\
                       .filter(PubStat.ts_spawn >= g.ts) \
                       .filter(PubStat.ts_spawn < g.te +
                                                  TimeStamp.DAYSECONDS - 1)
        elif account_group == Account.Groups.ADV:
            q = AdvStat.PreQuery()\
                       .filter(AdvStat.ts_spawn >= g.ts) \
                       .filter(AdvStat.ts_spawn < g.te +
                                                  TimeStamp.DAYSECONDS - 1)
        else:
            raise AdminStatError
        return q

    def _publisher_stats(self, elements):
        """Initial statistics for the publishers."""
        g.a_group = Account.Groups.PUB
        return self._stats(elements, Account.Groups.PUB)

    def _advertiser_stats(self, elements):
        """Initial statistics for the advertiser."""
        g.a_group = Account.Groups.ADV
        return self._stats(elements, Account.Groups.ADV)

    def _stats(self, elements, account_group):
        """Main method that initial statistics and builds a pipeline."""
        today = datetime.utcfromtimestamp(TimeStamp.today())\
            .strftime('%d.%m.%Y')
        g.group = request.values.get('group', '')
        g.account = request.values.get('account', '') or \
            request.values.get('advertiser', '') or \
            request.values.get('publisher', '')
        g.application = request.values.get('application', '')
        g.unit = request.values.get('unit', '')
        g.source = request.values.get('source', '')
        g.campaign = request.values.get('campaign', '')
        g.media = request.values.get('media', '')
        g.country = request.values.get('country', '')
        g.region = request.values.get('region', '')
        g.city = request.values.get('city', '')
        g.date_period = request.values.get('date', '') or today + ' - ' + today

        # Get name of the elements if they exist.
        for el in elements:
            el = 'account' if el in ('publisher', 'advertiser', ) else el
            if getattr(g, el, None):
                try:
                    if el in ('country', 'region', 'city',):
                        cls = globals().get('Geo' + el.capitalize())
                    elif el == 'group':
                        cls = globals().get('Partner' + el.capitalize())
                    else:
                        cls = globals().get(el.capitalize())
                    instance = cls.Get(getattr(g, el))
                    if el == 'account':
                        setattr(g, el + '_name', instance.email)
                    else:
                        setattr(g, el + '_name', instance.name)
                except StandardError:
                    pass

        pipeline = self._build_pipeline(elements, account_group, light=True)
        g.selects = [pipeline[e] for e in elements]

    @Route('/pub/days/')
    def publisher_days(self):
        """Shows common publishers stats."""
        g.page = 'days'
        self._publisher_stats(['group', 'publisher', 'application',
                               'unit', 'source', 'country', 'region', 'city'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/hours/')
    def publisher_hours(self):
        """Shows common publishers stats."""
        g.page = 'hours'
        self._publisher_stats(['group', 'publisher', 'application',
                               'unit', 'source', 'country', 'region', 'city'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/pub/')
    def publisher_pub(self):
        """Shows publishers stats regarding the publishers."""
        g.page = 'acc'
        self._publisher_stats(['group', 'publisher', 'country',
                               'region', 'city'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/app/')
    def publisher_app(self):
        """Shows publishers stats regarding the applications."""
        g.page = 'app'
        self._publisher_stats(['publisher', 'application',
                               'country', 'region', 'city'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/unit/')
    def publisher_unit(self):
        """Shows publishers stats regarding the units."""
        g.page = 'unit'
        self._publisher_stats(['publisher', 'application', 'unit',
                               'country', 'region', 'city'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/source/')
    def publisher_source(self):
        """Shows publishers stats regarding the sources."""
        g.page = 'source'
        self._publisher_stats(['publisher', 'application', 'unit', 'source',
                               'country', 'region', 'city'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/geo/country/')
    def publisher_geo_country(self):
        g.page = 'country'
        self._publisher_stats(['group', 'publisher', 'application',
                               'unit', 'source'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/geo/region/')
    def publisher_geo_region(self):
        g.page = 'region'
        self._publisher_stats(['group', 'publisher', 'application',
                               'unit', 'source', 'country'])
        return render_template('/stats/publisher/index.mako')

    @Route('/pub/geo/city/')
    def publisher_geo_city(self):
        g.page = 'city'
        self._publisher_stats(['group', 'publisher', 'application',
                               'unit', 'source', 'country', 'region'])
        return render_template('/stats/publisher/index.mako')

    @Route('/adv/days/')
    def advertiser_days(self):
        """Shows common publishers stats."""
        g.page = 'days'
        self._advertiser_stats(['group', 'advertiser', 'campaign',
                                'media', 'country', 'region', 'city'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/hours/')
    def advertiser_hours(self):
        """Shows common publishers stats."""
        g.page = 'hours'
        self._advertiser_stats(['group', 'advertiser', 'campaign',
                                'media', 'country', 'region', 'city'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/adv/')
    def advertiser_adv(self):
        """Shows common publishers stats."""
        g.page = 'acc'
        self._advertiser_stats(['group', 'advertiser', 'country',
                                'region', 'city'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/cam/')
    def advertiser_cam(self):
        """Shows common publishers stats."""
        g.page = 'cam'
        self._advertiser_stats(['group', 'advertiser',
                                'campaign', 'country', 'region', 'city'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/media/')
    def advertiser_media(self):
        """Shows common publishers stats."""
        g.page = 'media'
        self._advertiser_stats(['group', 'advertiser', 'campaign',
                                'media', 'country', 'region', 'city'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/geo/country/')
    def advertiser_geo_country(self):
        g.page = 'country'
        self._advertiser_stats(['group', 'advertiser', 'campaign',
                                'media'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/geo/region/')
    def advertiser_geo_region(self):
        g.page = 'region'
        self._advertiser_stats(['group', 'advertiser', 'campaign',
                                'media', 'country'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/adv/geo/city/')
    def advertiser_geo_city(self):
        g.page = 'city'
        self._advertiser_stats(['group', 'advertiser', 'campaign',
                                'media', 'country', 'region'])
        return render_template('/stats/advertiser/index.mako')

    @Route('/ajax/select')
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        account_group = int(request.values.get('account_group', ''))
        search = request.values.get('q', '')
        obj_list = []

        geo = ['country', 'region', 'city']
        v = values.split(',')
        if query not in geo:
            values = ','.join(
                [s for s in v if not any(map(lambda x: s.startswith(x), geo))])
        else:
            values = ','.join(
                [s for s in v if any(map(lambda x: s.startswith(x), geo))])
        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))
        pipeline = self._build_pipeline(pipeline_elements, account_group,
                                        select=query, search=search)
        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            select = pipeline.get(name)
            if not select:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': u"{name} (ID - {id})"\
                        .format(id=key, name=n)}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': u"{name} (ID - {id})"\
                        .format(id=key, name=n)}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id:
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]

        # TODO: The code below wraps in a specific function.
        if query == 'country' and not search:
            # The list below contains IDs of CIS countries and also their order.
            cis_ids = [1, 2, 7, 4, 6, 11, 39, 49, 50, 8, 9, 15, 27, 40]
            cis_countries = list(xrange(len(cis_ids)))
            other_counties = []
            for c in obj_list:
                if c.get('id') in cis_ids:
                    cis_countries[cis_ids.index(c.get('id'))] = c
                else:
                    other_counties.append(c)
            other_counties = sorted(other_counties,
                                    cmp=lambda x, y: cmp(x['name'].lower(),
                                                         y['name'].lower()))
            obj_list = cis_countries + other_counties
        else:
            obj_list = sorted(obj_list, cmp=lambda x, y: cmp(x['name'].lower(),
                                                             y['name'].lower()))
        obj_list = [{'id': None, 'name': u""}] + obj_list
        data = {"objects": obj_list}
        return jsonify(**data)

    @Route('/ajax/table')
    def ajax_table(self):
        page = request.values.get('page', '')
        account_group = int(request.values.get('account_group', ''))
        values = request.values.get('values', '')
        timezone = try_int(request.values.get('timezone'), -time.timezone)

        for name, v in map(lambda x: tuple(e for e in x.split(':')),
                           values.split(',')):
            name = 'account' if name in ('advertiser', 'publisher',) else name
            setattr(g, name, try_int(v, None))

        self._apply_filters(account_group, timezone, page)

        objects = []
        ts = []
        data = {'meta': {}}
        # Make assistant sets. This sets are used to fetch additional data,
        # e.g name or email, by IDs that are placed in the sets.
        # Initial sets.
        account_set = set()
        campaign_set = set()
        media_set = set()
        application_set = set()
        unit_set = set()
        source_set = set()
        country_set = set()
        region_set = set()
        city_set = set()

        for stat in g.stats:
            first = first_row = getattr(stat, page)
            try:
                crt = stat.clicks / float(stat.impressions) * 100
            except ZeroDivisionError:
                crt = '0'
            d = {'impressions': stat.impressions, 'clicks': str(stat.clicks),
                 'crt': str(crt)}

            if page == 'days':
                first = datetime.utcfromtimestamp(first_row + timezone)\
                    .strftime('%d.%m.%Y')
            elif page == 'hours':
                first = datetime.utcfromtimestamp(int(first_row) + timezone)\
                    .strftime('%d.%m.%Y %H:%M')
                # Store a list of TS for nullable entries.
                ts.append(int(first_row))
            elif page == 'acc':
                d['second'] = None
                account_set.add(first_row)
            elif page == 'cam':
                d['second'] = None
                campaign_set.add(first_row)
            elif page == 'media':
                d['second'] = d['third'] = None
                media_set.add(first_row)
            elif page == 'app':
                d['second'] = None
                application_set.add(first_row)
            elif page == 'unit':
                d['second'] = d['third'] = None
                unit_set.add(first_row)
            elif page == 'source':
                d['second'] = d['third'] = d['fourth'] = None
                source_set.add(first_row)
            elif page == 'country':
                country_set.add(first_row)
            elif page == 'region':
                region_set.add(first_row)
            elif page == 'city':
                city_set.add(first_row)

            d['first'] = first

            if account_group == Account.Groups.ADV:
                d.update({
                    'overlays': str(stat.overlays),
                    'overlay_cost': str(stat.overlay_cost),
                    'endcards': stat.endcards,
                    'endcard_cost': str(stat.endcard_cost),
                    'cost': str(stat.cost),
                })
            elif account_group == Account.Groups.PUB:
                try:
                    cpm = float(stat.revenue) / stat.impressions * 1000
                except ZeroDivisionError:
                    cpm = 0
                d.update({
                    'v4vc_cost': stat.v4vc_cost,
                    'cpm': cpm,
                    'revenue': str(stat.revenue),
                })
            else:
                raise AdminStatError

            objects.append(d)

        #TODO: The code below put in a function and refactoring.
        # Fetch the additional parameters via sets. The sets store IDs.
        if account_set:
            _accounts = Session.query(Account)\
                .filter(Account.id.in_(list(account_set))).all()
            account_dict = dict([(a.id, (a.email, a.partner_group_id))
                                 for a in _accounts])
            _groups = Session.query(PartnerGroup)\
                .filter(PartnerGroup.id.in_(
                list(set([a.partner_group_id for a in _accounts])))).all()
            group_dict = dict([(gr.id, gr.name) for gr in _groups])
            data['meta'].update({'account': account_dict, 'group': group_dict})
        if campaign_set:
            _campaigns = Session.query(Campaign)\
                .filter(Campaign.id.in_(list(campaign_set))).all()
            campaign_dict = dict([(c.id, (c.name, c.account)) for c in _campaigns])
            _accounts = Session.query(Account)\
                .filter(Account.id.in_(
                list(set([c.account for c in _campaigns])))).all()
            account_dict = dict([(a.id, a.email) for a in _accounts])
            data['meta'].update({'account': account_dict,
                                 'campaign': campaign_dict})
        if media_set:
            _medias = Session.query(Media)\
                .filter(Media.id.in_(list(media_set))).all()
            media_dict = dict([(m.id, (m.name, m.campaign)) for m in _medias])
            _campaigns = Session.query(Campaign)\
                .filter(Campaign.id.in_(
                list(set([m.campaign for m in _medias])))).all()
            campaign_dict = dict([(c.id, (c.name, c.account)) for c in _campaigns])
            _accounts = Session.query(Account)\
                .filter(Account.id.in_(
                list(set([c.account for c in _campaigns])))).all()
            account_dict = dict([(a.id, a.email) for a in _accounts])
            data['meta'].update({'account': account_dict, 'media': media_dict,
                                 'campaign': campaign_dict})
        if application_set:
            _apps = Session.query(Application)\
                .filter(Application.id.in_(list(application_set))).all()
            app_dict = dict([(ap.id, (ap.name, ap.account)) for ap in _apps])
            _accounts = Session.query(Account)\
                .filter(Account.id.in_(
                list(set([app.account for app in _apps])))).all()
            account_dict = dict([(a.id, a.email) for a in _accounts])
            data['meta'].update({'account': account_dict,
                                 'application': app_dict})
        if unit_set:
            _units = Session.query(Unit)\
                .filter(Unit.id.in_(list(unit_set))).all()
            unit_dict = dict([(x.id, (x.name, x.application)) for x in _units])
            _apps = Session.query(Application)\
                .filter(Application.id.in_(
                list(set([x.application for x in _units])))).all()
            app_dict = dict([(a.id, (a.name, a.account)) for a in _apps])
            _accounts = Session.query(Account)\
                .filter(Account.id.in_(
                list(set([a.account for a in _apps])))).all()
            account_dict = dict([(a.id, a.email) for a in _accounts])
            data['meta'].update({'account': account_dict, 'unit': unit_dict,
                                 'application': app_dict})
        if source_set:
            _sources = Session.query(Source.id, Source.name, Source.unit)\
                .filter(Source.id.in_(list(source_set))).all()
            source_dict = dict([(x.id, (x.name, x.unit))
                                for x in _sources])
            _units = Session.query(Unit)\
                .filter(Unit.id.in_(list(set([x.unit for x in _sources]))))\
                .all()
            unit_dict = dict([(x.id, (x.name, x.application)) for x in _units])
            _apps = Session.query(Application)\
                .filter(Application.id.in_(
                list(set([x.application for x in _units])))).all()
            app_dict = dict([(a.id, (a.name, a.account)) for a in _apps])
            _accounts = Session.query(Account)\
                .filter(Account.id.in_(
                list(set([a.account for a in _apps])))).all()
            account_dict = dict([(a.id, a.email) for a in _accounts])
            data['meta'].update({'account': account_dict, 'unit': unit_dict,
                                 'application': app_dict,
                                 'source': source_dict})
        if country_set:
            _countries = Session.query(GeoCountry)\
                .filter(GeoCountry.id.in_(list(country_set))).all()
            country_dict = dict([(c.id, c.name) for c in _countries])
            data['meta'].update({'country': country_dict})
        if region_set:
            _regions = Session.query(GeoRegion)\
                .filter(GeoRegion.id.in_(list(region_set))).all()
            region_dict = dict([(r.id, r.name) for r in _regions])
            data['meta'].update({'region': region_dict})
        if city_set:
            _cities = Session.query(GeoCity)\
                .filter(GeoCity.id.in_(list(city_set))).all()
            city_dict = dict([(c.id, c.name) for c in _cities])
            data['meta'].update({'city': city_dict})

        if page in ('days', 'hours', ):
            def cmp_date(x, y):
                t = x.split(' ')
                t[0] = t[0].split('.')
                t[0].reverse()
                t[0] = '.'.join(t[0])
                t = ' '.join(t)
                v = y.split(' ')
                v[0] = v[0].split('.')
                v[0].reverse()
                v[0] = '.'.join(v[0])
                v = ' '.join(v)
                return cmp(t, v)

            objects.sort(cmp=lambda x, y: cmp_date(x['first'], y['first']))

        # Insert the nullable hours.
        # When we will want to delete this strange feature that is useless
        # we will need only to delete "if" block below.
        if page in ('hours', ):
            ts = sorted(ts)
            hours = list(xrange(g.ts, g.te + TimeStamp.DAYSECONDS, 3600))
            # Make bit mask in order to determine that hours are.
            mask = [True if h in ts else False for h in hours]
            empty_entry = {
                'first': 0,
                'impressions': 0,
                'clicks': 0,
                'crt': 0,
            }
            if account_group == Account.Groups.PUB:
                empty_entry.update({
                    'v4vc_cost': 0,
                    'cpm': 0,
                    'revenue': '0',
                })
            elif account_group == Account.Groups.ADV:
                empty_entry.update({
                    'overlays': 0,
                    'overlay_cost': 0,
                    'endcards': 0,
                    'endcard_cost': 0,
                    'cost': '0',
                })
            else:
                raise AdminStatError
            _objects = []
            for i, m in enumerate(mask):
                if m:
                    _objects.append(objects.pop(0))
                else:
                    h = datetime.utcfromtimestamp(hours[i] + timezone)\
                        .strftime('%d.%m.%Y %H:%M')
                    entry = empty_entry.copy()
                    entry['first'] = h
                    _objects.append(entry)
            data = {'data': _objects}
            return jsonify(**data)

        data.update({'data': objects})

        return jsonify(**data)