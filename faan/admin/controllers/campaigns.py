# -*- coding: utf-8 -*-
import json
from decimal import Decimal
from sqlalchemy import or_
from sqlalchemy.sql.expression import asc, and_
from sqlalchemy.sql.functions import sum as sqlsum
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model.category import Category
from faan.core.model.meta import Session
from faan.core.model.security.account import Account
from faan.core.model.security.partner_group import PartnerGroup
from faan.web.forms.advertiser import CampaignForm
from faan.web.lib.countrynodes import generateNodes
from faan.core.model.adv.forecast import ForecastStat
from faan.x.util import try_int


def _get_total_new_campaign(ctype=None):
    try:
        new_campaign = Session.query(Campaign).filter(Campaign.state ==
                                                      Campaign.State.NONE)
        if ctype is not None:
            new_campaign = new_campaign.filter(Campaign.type == ctype)
        return new_campaign.count()
    except Exception:
        return 0


def get_total_new_campaign():
    return _get_total_new_campaign()


class Tabs(object):
    NEW = 1
    ACTIVE = 2
    BANNED = 3

    @classmethod
    def tabs(cls):
        return cls.ACTIVE, cls.BANNED


@CProtect(IsSigned())
@CProtect(permissions.campaigns)
@Controller('/campaign')
class CampaignController(object):
    BASEBID = 1.0

    @Route("/")
    def index(self):
        g.group = request.values.get('group', '')
        g.account = request.values.get('account', '')
        g.tab = try_int(request.values.get('tab'), Tabs.ACTIVE)
        elements = ['group', 'account']

        if g.account:
            account = Account.Key(g.account)
            g.account_name = account.email

        if g.group:
            group = PartnerGroup.Key(g.group)
            g.group_name = group.name

        pipeline = self._build_pipeline(elements, light=True)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/campaign/index.mako')

    @Route("/<int:cmp_id>", methods=["GET", "POST"])
    def form(self, cmp_id=None):
        g.campaign = Campaign.Single(id=cmp_id) or Campaign.Default()
        form = CampaignForm(request.form, obj=g.campaign)

        if request.method == "POST" and form.validate():

            g.campaign.name                     = form.data.get('name')
            g.campaign.category                 = form.data.get('category')

            g.campaign.limit_cost_total         = form.data.get('limit_cost_total')
            g.campaign.limit_cost_daily         = form.data.get('limit_cost_daily')

            g.campaign.ts_start                 = form.data.get('ts_start')
            g.campaign.ts_end                   = form.data.get('ts_end')

            g.campaign.targeting                = reduce( lambda acc, v: acc | int(v)
                                                        , request.values.getlist('targeting')
                                                        , 0)

            g.campaign.target_categories_groups = form.data.get('target_categories_groups', [])

            campaign_categories = Session.query(Category.related_categories)\
                .filter(Category.id.in_(g.campaign.target_categories_groups))\
                .all()

            target_categories = []
            for c in campaign_categories:
                target_categories += c.related_categories

            g.campaign.target_categories        = list(set(target_categories))
            g.campaign.target_platforms         = form.data.get('target_platforms', [])
            g.campaign.target_devclasses        = form.data.get('target_devclasses', [])
            g.campaign.target_geo               = form.data.get('target_geo', [])
            g.campaign.target_content_rating    = form.data.get('target_content_rating', [])
            g.campaign.target_age               = form.target_age.data
            g.campaign.target_gender            = form.target_gender.data

            g.campaign.bid                      = Decimal(form.data.get('bid'))
            g.campaign.total_cost_limit         = request.values.get('total_cost_limit')

            g.campaign.type                     = g.campaign.type or Campaign.Type.REGULAR
            g.campaign.state                    = g.campaign.state or Campaign.State.ACTIVE

            try:
                Session.add(g.campaign)
                Session.flush()
                Session.commit()
                return redirect("/campaign/?tab=" + str(Tabs.ACTIVE))
            except:
                capp.logger.exception('EXCEPTION IN /campaign/form')

        g.categories = Category.All()
        g.nodes = generateNodes()

        return render_template('/campaign/form.mako', form=form)

    @Route("/forecast", methods=["POST"])
    def forecast(self):
        targeting = reduce(lambda acc, v: acc | int(v), # ["2", "4", ...] -> 2 | 4 | ...
                           request.values.getlist('targeting'),
                           0)

        target_categories       = map(int, request.values.getlist('target_categories'))   # ["1", "5", ...] -> [1, 5, ...]
        target_platforms        = map(int, request.values.getlist('target_platforms'))
        target_devclasses       = map(int, request.values.getlist('target_devclasses'))
        target_content_ratings  = map(int, request.values.getlist('target_content_ratings'))
        target_geos             = map(int, request.values.getlist('target_geos'))

        bid = int(Decimal(request.values.get('bid') or 0) * 10)

        result = {
            "min"         : 0,
            "recommended" : 0,
            "forecast"    : 0,
        }

        result["min"] = CampaignController.BASEBID * (1 + len(request.values.getlist('targeting')) * 0.1)

        # CONSTRUCT QUERY

        q = Session.query(ForecastStat.cost_index,
                          (sqlsum(ForecastStat.impressions)).label('impressions'))

        if targeting & Campaign.Targeting.CATEGORY:
            q = q.filter(and_( ForecastStat.category.in_(target_categories)
                             , ForecastStat.content_rating.in_(target_content_ratings) ))

        if targeting & Campaign.Targeting.DEVCLASS:
            q = q.filter(ForecastStat.devclass.in_(target_devclasses))

        if targeting & Campaign.Targeting.PLATFORM:
            q = q.filter(ForecastStat.platform.in_(target_platforms))

        q = q.group_by(ForecastStat.cost_index)
        q = q.order_by(asc(ForecastStat.cost_index))

        data = q.all()

        if data:
            total = sum([imps for (idx, imps) in data])

            result["recommended"] = "%.2f" % (sum([(1.0 * idx * imps) / total for (idx, imps) in data]) / 10.0)
            result["forecast"] = sum([imps for (idx, imps) in data if idx <= bid])

        return jsonify(result)

    @Route("/category", methods=["GET"])
    def categories_list(self):
        platform = request.values.get("platform")
        ctype = request.values.get("type")
        obj_list = []

        search = request.values.get("q")

        q = Session.query(Category.name, Category.id)

        if platform:
            q = q.filter(Category.platform == platform)

        if ctype:
            q = q.filter(Category.type == ctype)

        if search and search is not "":
            q = q.filter(Category.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": z.id, "name": z.name} for z in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/select")
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]

        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))

        pipeline = self._build_pipeline(pipeline_elements, select=query,
                                        search=search)

        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            select = pipeline.get(name)
            if not select:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id and _id.isdigit():
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)
    
    @Route("/ajax/table")
    def ajax_table(self):
        values = request.values.get('values', '')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')
        g.tab = try_int(request.values.get('tab'))
        objects = []

        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            setattr(g, name, _id if _id and _id.isdigit() else None)

        self._filter_campaign(start=start, count=count, search=search)

        cam_ids = [c.id for c, _, __ in g.data]
        if cam_ids:
            medias = Session.query(Media)\
                .filter(Media.campaign.in_(cam_ids)).all()
            media_dict = dict([(c, []) for c in cam_ids])
            for m in medias:
                media_dict[m.campaign].append({'id': m.id, 'name': m.name,
                                               'state': m.state})
        else:
            media_dict = {}

        for c, a, gr in g.data:
            campaign = {
                'id': c.id,
                'name': c.name,
                'a_id': a.id,
                'a_name': a.email,
                'g_id': gr.id,
                'g_name': gr.name,
                'categories': "",
            }
            d = {
                'checkbox': c.id,
                'campaign': json.dumps(campaign),
                'state': c.state,
                'medias': json.dumps(media_dict[c.id]) if media_dict else "",
                'payment_type': c.payment_type,
                'bid': c.bid,
                'devices': json.dumps(c.target_devclasses),
                'actions': json.dumps({'id': c.id, 'state': c.state}),
            }
            objects.append(d)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)
    
    @Route("/active/<int:cam_id>")
    def active_cam(self, cam_id=None):
        g.tab = try_int(request.values.get('tab', Tabs.ACTIVE))
        self._active_cam(cam_id)
        return redirect('/campaign/?tab=' + g.tab)

    @Route("/disable/<int:cam_id>")
    def disable_cam(self, cam_id=None):
        g.tab = try_int(request.values.get('tab', Tabs.ACTIVE))
        self._disable_cam(cam_id)
        return redirect('/campaign/?tab=' + g.tab)

    @Route("/group_actions", methods=["POST"])
    def group_actions(self):
        campaigns = request.values.get('objects_list')
        action = request.values.get('action')

        method = {
            'active': self._active_cam,
            'disable': self._disable_cam,
            'delete': self._delete_cam,
        }.get(action)

        if not method:
            return "{}"

        for cam in list(set(map(int, campaigns.split(',')))):
            method(cam, commit=False)

        Session.flush()
        Session.commit()

        return "{}"
    
    def _disable_cam(self, cam_id, commit=True):
        Session.query(Campaign).filter(Campaign.id == cam_id)\
            .update({"state": Campaign.State.DISABLED})
        if commit:
            Session.commit()

    def _active_cam(self, cam_id, commit=True):
        Session.query(Campaign).filter(Campaign.id == cam_id)\
            .update({"state": Campaign.State.ACTIVE})
        if commit:
            Session.commit()

    def _delete_cam(self, cam_id, commit=True):
        cam = Campaign.Get(cam_id)
        Session.delete(cam)
        if commit:
            Session.flush()
            Session.commit()
    
    def _filter_campaign(self, start=None, count=None, search=None):
        q = Session.query(Campaign, Account, PartnerGroup)\
            .filter(Account.id == Campaign.account)\
            .filter(Account.partner_group_id == PartnerGroup.id)\
            .order_by(Campaign.id.desc())

        if g.account and g.account.isdigit():
            q = q.filter(Account.id == g.account)

        if g.group and g.group.isdigit():
            q = q.filter(PartnerGroup.id == g.group)

        q = {
            Tabs.NEW: q.filter(Campaign.state == Campaign.State.NONE),
            Tabs.ACTIVE: q.filter(Campaign.state == Campaign.State.ACTIVE),
            Tabs.BANNED: q.filter(Campaign.state == Campaign.State.DISABLED),
        }.get(g.tab)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Campaign.name.ilike(u'%{0}%'.format(search)),
                    PartnerGroup.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Campaign.id == search,
                    Campaign.bid == search,
                ))
            else:
                q = q.filter(or_(
                    Campaign.name.ilike(u'%{0}%'.format(search)),
                    PartnerGroup.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    def _build_dicts(self, select=None, search=None):
        """Builds dictionaries."""
        q = Session.query(Account).order_by(Account.id)
        if select == 'account':
            q = q.filter(Account.email.ilike(u'%{0}%'.format(search)))
        g.accounts = q.all()

        q = Session.query(PartnerGroup)
        if select == 'group':
            q = q.filter(PartnerGroup.name.ilike(u'%{0}%'.format(search)))
        g.groups = q.all()

        g.groups_dict = dict([(gr.id, (str(gr.account_type) +
                                       ' - ' + gr.name, u''))
                              for gr in g.groups])
        g.accounts_dict = dict([(account.id, (account.email,
                                              account.partner_group_id))
                                for account in g.accounts])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements, light=False, select=None,
                        search=None):
        pipeline = {}
        if not light:
            self._build_dicts(select=select, search=search)

        store = {
            'group': {
                'name': 'group',
                'dict': None if light else g.groups_dict,
            },
            'account': {
                'name': 'account',
                'dict': None if light else g.accounts_dict,
            },
        }

        for el in pipeline_elements:
            store[el]['id'] = "select_" + store[el]['name']
            pipeline[el] = store[el]
        return pipeline