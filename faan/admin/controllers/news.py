# coding=utf-8
import datetime
import json
import time

from flask import request, g, redirect, jsonify
from sqlalchemy.dialects.postgresql import Any
from faan.admin.forms.news import NewsEditForm
from faan.admin.controllers import permissions

from faan.core.X.xflask import Controller, Route, capp
from faan.core.model.news import News
from faan.core.model.security import Account
from faan.core.model.meta import Session
from faan.core.X.mako import render_template
from faan.admin.lib.security import IsSigned
from faan.core.model.tag import Tag
from faan.x.security import CProtect
from faan.x.util import try_int

__author__ = 'limeschnaps'


def get_now_timestamp():
    return int(time.mktime(datetime.datetime.utcnow().timetuple()))


@CProtect(IsSigned())
@CProtect(permissions.news)
@Controller("/news")
class NewsController(object):
    @Route("/")
    def index_news_view(self):
        g.priorities = {
            News.Priority.LOW: (u"Низкая", "info"),
            News.Priority.NORMAL: (u"Обычная", "success"),
            News.Priority.HIGH: (u"Высокая", "warning"),
            News.Priority.CRITICAL: (u"Критическая", "danger"),
        }

        g.tags = {t.id: t for t in Session.query(Tag).all()}

        g.current_tag = try_int(request.values.get("tag"))

        query = Session.query(News)

        if g.current_tag:
            query = query.filter(Any(g.current_tag, News.tags))

        g.news_list = query.order_by(News.ts_created).all()

        g.account_names = {a.id: a.username for a in Session.query(Account.username, Account.id).filter(Account.id.in_([x.created_by for x in g.news_list])).all()}

        return render_template("news/news_list.mako")

    @Route("/new", methods=["GET", "POST"])
    @Route("/<int:news_id>", methods=["GET", "POST"])
    def edit_news_view(self, news_id=None):
        if news_id:
            news_obj = News.Key(news_id)
        else:
            news_obj = News.Default()

        form = NewsEditForm(request.form, obj=news_obj)

        g.tags = Session.query(Tag).all()

        if request.method == "POST" and form.validate():
            if form.tags.data:
                existing_tags = Session.query(Tag).filter(Tag.name.in_(form.tags.data)).all()
                existing_tags_names = [x.name for x in existing_tags]
            else:
                existing_tags = existing_tags_names = []

            new_tags = []
            new_tags_names = [tag for tag in form.tags.data if tag not in existing_tags_names]

            for tag_name in new_tags_names:
                new_tag = Tag.Default()
                new_tag.name = tag_name
                new_tags.append(new_tag)
                Session.add(new_tag)

            Session.flush()
            Session.commit()

            all_tags = existing_tags + new_tags

            form.populate_obj(news_obj)

            news_obj.tags = [x.id for x in all_tags]
            news_obj.created_by = request.environ["REMOTE_USER"]
            news_obj.ts_created = get_now_timestamp()

            try:
                Session.add(news_obj)
                Session.flush()
                Session.commit()
                return redirect("/news")
            except:
                capp.logger.exception("While saving new news item")

        return render_template("news/news_edit.mako", form=form)
