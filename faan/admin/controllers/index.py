# -*- coding: utf-8 -*-
# IMPORTS
# --START
from flask.helpers import send_from_directory

# =========

from sqlalchemy.sql import func
from flask import request, jsonify, redirect, g
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, Error, capp
from faan.core.X.mako import render_template
from faan.core.model.meta import Session

from faan.core.model.pub.statistics import PubStat
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.pub.application import Application
from faan.core.model.adv.campaign import Campaign
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned, IsAdmin
import decimal
import time
import operator
from datetime import datetime
from calendar import timegm
from faan.x.util.timestamp import TimeStamp
from faan.x.util import try_int

# =========

from os.path import abspath, join, curdir

# --END


# UTILS
# --START
def prejson_dict(l):
    return [{k: float(v) if isinstance(v, decimal.Decimal) else v
             for k, v in d.iteritems()} for d in l]


def sort(q, cond):
    return sorted([x._asdict() for x in q], key=operator.itemgetter(cond))


def ts_utc_json(ts, tz=0):
    if tz:
        ts -= tz
    return int("%d000" % ts)
# --END


@Controller
class GlobalStaticController(object):
    @Route("/global/<path:path>")
    def global_serve(self, path):
        return send_from_directory(join(abspath(curdir),
                                        capp.config["GLOBAL_FOLDER"]), path)


@CProtect(IsSigned())
@CProtect(permissions.index)
@Controller
class IndexController(object):
    @Route("/")
    def index(self, ex = None):
        g.num_top_apps = try_int(request.values.get('num_top_apps'), 10)
        g.num_top_cams = try_int(request.values.get('num_top_cams'), 10)
        return render_template('index/index.mako')

    @Route("/ajax/dashboard")
    def stat(self):
        #user = request.environ['REMOTE_USER']  # Unused just for now
        timezone = try_int(request.values.get('timezone'), -time.timezone)
        g.num_top_apps = try_int(request.values.get('num_top_apps'), 10)
        g.num_top_cams = try_int(request.values.get('num_top_cams'), 10)

        try:
            ts, te = request.values['date'].split(' - ')
            ts = timegm(datetime.strptime(ts, "%m/%d/%Y").timetuple())
            te = timegm(datetime.strptime(te, "%m/%d/%Y").timetuple())
            ts, te = ts - timezone, te - timezone
        except (KeyError, IndexError, ValueError, NameError, TypeError):
            ts = TimeStamp.today_tz() + time.timezone
            te = TimeStamp.today_tz() + time.timezone

        if ts - te:
            grouping = (
                ((PubStat.ts_spawn + timezone) - (PubStat.ts_spawn + timezone)
                 % (TimeStamp.DAYSECONDS)),
                ((AdvStat.ts_spawn + timezone) - (AdvStat.ts_spawn + timezone)
                 % (TimeStamp.DAYSECONDS))
            )

            tz = 0
            hourly = False
            cpm_mult = 2000
            tss = list(xrange(ts + timezone, te + timezone +
                              TimeStamp.DAYSECONDS, TimeStamp.DAYSECONDS))
        else:
            grouping = (PubStat.Presets.GROUP_HOURLY,
                        AdvStat.Presets.GROUP_HOURLY)
            tz = -timezone
            hourly = True
            cpm_mult = 100
            tss = list(xrange(ts, te + TimeStamp.DAYSECONDS, 3600))

        # Get top applications
        app_ids, app_names, app_top_dict = self.top_apps(ts, te, tss,
                                                         grouping[0])
        q = PubStat.PreQuery()\
            .filter(PubStat.ts_spawn >= ts) \
            .filter(PubStat.ts_spawn < te + TimeStamp.DAYSECONDS)

        q_table = q.add_column(grouping[0].label('ts_spawn'))\
            .group_by(grouping[0])
        q_table = prejson_dict(sort(q_table.all(), 'ts_spawn'))

        # Get top campaigns
        cam_ids, cam_names, cam_top_dict = self.top_campaigns(ts, te, tss,
                                                              grouping[1])
        q = AdvStat.PreQuery()\
            .filter(AdvStat.ts_spawn >= ts) \
            .filter(AdvStat.ts_spawn < te + TimeStamp.DAYSECONDS)

        q = q.add_column(grouping[1].label('ts_spawn')).group_by(grouping[1])

        mrg = self.margin(ts, te, tss, grouping)

        clicks = {x.get("ts_spawn"): x.get("clicks") for x in prejson_dict(sort(q.all(), 'ts_spawn'))}

        for item in q_table:
            item.update({
                "date": ts_utc_json(item.get("ts_spawn", 0)),
                "cpm": "%.2f" % (float(item.get("revenue")) /
                                 item.get("impressions") * 1000)
                                 if item.get("impressions") else 0,
                "ctr": "%.2f" % (float(clicks.get(item.get("ts_spawn"))) /
                                 item.get("impressions") * 100)
                                 if item.get("impressions") else 0,
                "clicks": clicks.get(item.get("ts_spawn"))
            })

        data = {
            "meta": {
                "hourly": hourly,
                "cpm_mult": cpm_mult,
                "date_from": datetime.fromtimestamp(ts).strftime('%m/%d/%Y'),
                "date_to": datetime.fromtimestamp(te).strftime('%m/%d/%Y')
            },
            "objects": {
                "graph_1": [
                    {
                        "label": u"Реквесты",
                        "data": [[ts_utc_json(x.get("ts_spawn", 0), tz), x.get("requests")] for x in q_table]
                    },
                    {
                        "label": u"Просмотров",
                        "data": [[ts_utc_json(x.get("ts_spawn", 0), tz), x.get("impressions")] for x in q_table]
                    }
                ],
                "graph_2": [
                    {
                        "label": u"Прибыль",
                        "data": [[ts_utc_json(ts, tz), m] for ts, m, _ in mrg]
                    },
                    {
                        "label": u"eCPM",
                        "data": [[ts_utc_json(ts, tz), e] for ts, _, e in mrg]
                    }
                ],
                "top_apps": list(xrange(len(app_ids))),
                "top_campaigns": list(xrange(len(cam_ids))),
                "table": q_table
            }
        }
        # Fill
        for app_id, app_list in app_top_dict.items():
            if app_list:
                data['objects']['top_apps'][app_ids.index(app_id)] = {
                    'label': app_names[app_id],
                    'data': [(ts_utc_json(_ts, tz), v) for _ts, v in app_list],
                }
        for cam_id, cam_list in cam_top_dict.items():
            if cam_list:
                data['objects']['top_campaigns'][cam_ids.index(cam_id)] = {
                    'label': cam_names[cam_id],
                    'data': [(ts_utc_json(_ts, tz), v) for _ts, v in cam_list],
                }
        if not data['objects']['top_apps']:
            data['objects']['top_apps'] = [{"label": u"", "data": []}]
        if not data['objects']['top_campaigns']:
            data['objects']['top_campaigns'] = [{"label": u"", "data": []}]


        return jsonify(**data)

    def top_apps(self, ts, te, tss, grouping):
        """ Determines TOP applications. """
        xq = Session.query(func.sum(PubStat.impressions).label('impressions'),
                                PubStat.application)\
            .filter(PubStat.ts_spawn >= ts) \
            .filter(PubStat.ts_spawn < te + TimeStamp.DAYSECONDS)\
            .group_by(PubStat.application)\
            .order_by("impressions desc").limit(g.num_top_apps)
        app_ids = [app for _, app in xq.all()]

        xq = PubStat.PreQuery()\
            .filter(PubStat.ts_spawn >= ts) \
            .filter(PubStat.ts_spawn < te + TimeStamp.DAYSECONDS)

        # Generate points for the TOP application chart.
        if app_ids:
            app_top_dict = dict([(a, []) for a in app_ids])
            app_names = dict(Session.query(Application.id, Application.name)\
                             .filter(Application.id.in_(app_ids)).all())
            app_top = xq.add_column(grouping.label('ts_spawn'))\
                .add_column(PubStat.application.label('app'))\
                .filter(PubStat.application.in_(app_ids))\
                .group_by(grouping, PubStat.application)\
                .order_by("ts_spawn").all()

            for app in app_top:
                app_top_dict[app.app].append((app.ts_spawn, app.impressions))

            # Add null values to specific application chart where
            # values don't exist.
            for app_list in app_top_dict.values():
                for i, _ts in enumerate(tss):
                    if i >= len(app_list):
                        app_list.append((_ts, 0))
                        continue
                    if app_list[i][0] != _ts:
                        app_list.insert(i, (_ts, 0))
        else:
            app_top_dict = {}
            app_names = {}

        return app_ids, app_names, app_top_dict

    def top_campaigns(self, ts, te, tss, grouping):
        """ Determines TOP campaigns. """
        xq = Session.query(func.sum(AdvStat.impressions).label('impressions'),
                           AdvStat.campaign)\
            .filter(AdvStat.ts_spawn >= ts) \
            .filter(AdvStat.ts_spawn < te + TimeStamp.DAYSECONDS)\
            .group_by(AdvStat.campaign)\
            .order_by("impressions desc").limit(g.num_top_cams)
        cam_ids = [c for _, c in xq.all()]

        xq = AdvStat.PreQuery()\
            .filter(AdvStat.ts_spawn >= ts) \
            .filter(AdvStat.ts_spawn < te + TimeStamp.DAYSECONDS)

        # Generate points for the TOP campaign chart.
        if cam_ids:
            cam_top_dict = dict([(c, []) for c in cam_ids])
            cam_names = dict(Session.query(Campaign.id, Campaign.name)
                             .filter(Campaign.id.in_(cam_ids)).all())
            cam_top = xq.add_column(grouping.label('ts_spawn'))\
                .add_column(AdvStat.campaign.label('campaign'))\
                .filter(AdvStat.campaign.in_(cam_ids))\
                .group_by(grouping, AdvStat.campaign)\
                .order_by("ts_spawn").all()
            for c in cam_top:
                cam_top_dict[c.campaign].append((c.ts_spawn, c.impressions))

            # Add null values to specific campaign chart where
            # values don't exist.
            for cam_list in cam_top_dict.values():
                for i, _ts in enumerate(tss):
                    if i >= len(cam_list):
                        cam_list.append((_ts, 0))
                        continue
                    if cam_list[i][0] != _ts:
                        cam_list.insert(i, (_ts, 0))
        else:
            cam_top_dict = {}
            cam_names = {}

        return cam_ids, cam_names, cam_top_dict

    def margin(self, ts, te, tss, grouping):

        query_pub = PubStat.PreQuery()\
            .filter(PubStat.ts_spawn >= ts)\
            .filter(PubStat.ts_spawn < te + TimeStamp.DAYSECONDS - 1)

        query_adv = AdvStat.PreQuery()\
            .filter(AdvStat.ts_spawn >= ts)\
            .filter(AdvStat.ts_spawn < te + TimeStamp.DAYSECONDS - 1)

        query_pub = query_pub.add_column(grouping[0].label('ts'))\
            .group_by(grouping[0])\
            .order_by(grouping[0])

        query_adv = query_adv.add_column(grouping[1].label('ts'))\
            .group_by(grouping[1])\
            .order_by(grouping[1])

        pub = query_pub.all()
        adv = query_adv.all()

        revenue = []
        for p, a in zip(pub, adv):
            m = a.cost - p.revenue
            ecpm = m / p.impressions if p.impressions != 0 else 0
            revenue.append((p.ts, m, ecpm))
        return revenue
