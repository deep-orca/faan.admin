from faan.admin.controllers import permissions
__author__ = 'limeschnaps'

from flask import g, request

from faan.core.X.xflask import Controller, Route
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.core.model.security import Account

@CProtect(IsSigned())
@CProtect(permissions.balance)
@Controller("/balance")
class BalanceController(object):
    @Route("/")
    def index(self):
        uid = request.environ["REMOTE_USER"]
        g.account = Account.Get(uid)
        return render_template('balance/index.mako')