# -*- encoding: utf-8 -*-

from flask import g, request, redirect, session

from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template

from faan.core.model.security import Account

from faan.core.model.meta import Session
from faan.x.util import try_int
from repoze.who.api import get_api
from faan.web.forms.sign import RegisterForm, SignInForm, RestoreForm, SignInFormCaptcha


@Controller('/sign')
class SignController(object):
    @Route("/in", methods=["GET", "POST"])
    def xin(self, ex=None):
        g.attempts = session.get("SignInAttempts", 1)
        if session.get("SignInAttempts", 0) >= 3:
            form = SignInFormCaptcha(request.form)
        else:
            form = SignInForm(request.form)

        who_api = get_api(request.environ)

        _error_list = {
            -2: {"message": u"Доступ запрещен", "class": "alert-danger"},
            -1: {"message": u"Неверный логин/пароль", "class": "alert-danger"},
            0: {"message": u"Ваш аккаунт находится на рассмотрении", "class": "alert-warning"},
            2: {"message": u"Ваш аккаунт заблокирован", "class": "alert-danger"},
            3: {"message": u"Действие вашего аккаунта временно приостановлено", "class": "alert-warning"}
        }

        if request.method == "POST" and form.validate():
            authenticated, headers = who_api.login({
                'login': form.data.get("email"),
                'password': Account.PassHash(form.data.get("password"))
            })

            if authenticated:
                account = Session().query(Account.groups, Account.state, Account.type)\
                    .filter(Account.id == authenticated.get("repoze.who.userid"))\
                    .all()[0]

                if account.state == Account.State.ACTIVE:
                    if account.type != Account.Type.PARTNER:
                        resp = redirect("/")
                        resp.headers.extend(headers)
                        return resp
                    else:
                        form.errors.update({"auth": _error_list.get(-2)})
                else:
                    error = _error_list.get(account.state)
                    form.errors.update({"auth": error})
            else:
                form.errors.update({"auth": _error_list.get(-1)})

            if len(form.errors):
                attempts = session.get("SignInAttempts", 1)
                attempts += 1
                session["SignInAttempts"] = attempts
            
        return render_template('sign/in.mako', form=form)

    @Route("/out", methods=["GET", "POST"])
    def xout(self, ex=None):
        who_api = get_api(request.environ)
        hs = who_api.logout()
        
        resp = redirect("/")
        resp.headers.extend(hs)
        return resp
        
    @Route("/restore", methods=["GET", "POST"])
    def restore(self, ex=None):
        form = RestoreForm(request.form)
        who_api = get_api(request.environ)

        if request.method == "POST" and form.validate():
            # TODO: Restore password logic
            pass

        return render_template('sign/restore.mako', form=form)