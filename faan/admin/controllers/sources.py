# -*- coding: utf-8 -*-
import json
from sqlalchemy import or_
from flask import g, request, redirect, jsonify
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.core.model.meta import Session
from faan.core.model.pub.application import Application
from faan.core.model.security.account import Account
from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source import Source
from faan.admin.forms.sources import SourceForm
from faan.x.util import try_int


class Tabs(object):
    NEW = 1
    ACTIVE = 2
    BANNED = 3

    @classmethod
    def tabs(cls):
        return cls.NEW, cls.ACTIVE, cls.BANNED


@CProtect(IsSigned())
@CProtect(permissions.source)
@Controller('/source')
class SourceController(object):

    @Route("/")
    def index(self):
        g.account = request.values.get('account', '')
        g.application = request.values.get('application', '')
        g.unit = request.values.get('unit', '')
        g.tab = try_int(request.values.get('tab'), Tabs.ACTIVE)
        elements = ['account', 'application', 'unit']

        if g.account:
            account = Account.Key(g.account)
            g.account_name = account.email

        if g.application:
            app = Application.Key(g.application)
            g.application_name = app.name

        if g.unit:
            unit = Unit.Key(g.unit)
            g.unit_name = unit.name

        pipeline = self._build_pipeline(elements, light=True)
        g.selects = [pipeline[e] for e in elements]
        return render_template('/source/index.mako')

    @Route('/<int:unit_id>/new', methods=['GET', 'POST'])
    @Route('/<int:unit_id>/<int:source_id>', methods=["GET", "POST"])
    def form(self, unit_id, source_id=None):
        source_type = request.values.get('type')
        g.unit = Unit.Key(unit_id)
        g.source = source = Source.Get(source_id) or Source.Default()
        # The source must have a type.
        if not source_id:
            g.source.type = source.type = int(source_type)
        else:
            source_type = source.type

        form = SourceForm(request.form, obj=source)

        if request.method == 'POST' and form.validate():
            app = Application.Key(g.unit.application)
            form.fill_source(app, g.unit, source)
            try:
                Session.add(source)
                Session.flush()
                Session.commit()
                return redirect('/unit/{0}/source'.format(g.unit.id))
            except StandardError:
                capp.logger.exception('EXCEPTION IN /unit/source/form')

        return render_template('/source/form.mako', form=form,
                               source_type=str(source_type))

    @Route("/ajax/select")
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]
        source_filters = ['source_type']

        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))
        pipeline_elements = [pe for pe in pipeline_elements
                             if pe not in source_filters]

        pipeline = self._build_pipeline(pipeline_elements, select=query,
                                        search=search)
        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            select = pipeline.get(name)
            if not select:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': n}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id and _id.isdigit():
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]
        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/ajax/table")
    def ajax_table(self):
        g.tab = try_int(request.values.get('tab'), Tabs.ACTIVE)
        values = request.values.get('values', '')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')

        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            setattr(g, name, _id if _id and _id.isdigit() else None)

        self.apply_filters(start=start, count=count, search=search)

        objects = []
        for s, unit, app, acc in g.data:
            dsource = {
                'id': s.id,
                'name': s.name,
                'unit_id': unit.id,
                'unit_name': unit.name,
                'app_id': app.id,
                'app_name': app.name,
                'acc_id': acc.id,
                'acc_email': acc.email,
            }
            d = {
                'checkbox': s.id,
                'source': dsource,
                'source_type': s.type,
                'state': s.state,
                'parameters': s.parameters,
                'ecpm': s.ecpm,
                'action': {'state': s.state, 'id': s.id},
            }
            objects.append(d)

        data = {
            'meta': {'total': g.total},
            'data': objects
        }

        return jsonify(**data)

    @Route("/<int:unit_id>/<int:source_id>/state/<int:state>")
    def state(self, source_id, unit_id, state):
        obj = Source.Key(source_id)
        obj.state = state

        try:
            Session.add(obj)
            Session.flush()
            Session.commit()
        except StandardError:
            capp.logger.exception("While setting source state")

        return redirect("/unit/{0}/source".format(unit_id))

    @Route("/<int:unit_id>/<int:source_id>/delete")
    def delete(self, source_id, unit_id):
        obj = Source.Key(source_id)

        try:
            Session.delete(obj)
            Session.flush()
            Session.commit()
        except StandardError:
            capp.logger.exception("While deleting source")

        return redirect("/unit/{0}/source".format(unit_id))

    @Route("/actions", methods=["POST"])
    def group_actions(self):
        sources = request.values.get('objects_list')
        action = request.values.get('action')

        method = {
            'active': self.active_source,
            'disable': self.disable_source,
            'delete': self.delete_source,
        }.get(action)

        if not method:
            return "{}"

        for m in list(set(sources.split(','))):
            method(m, commit=False)

        Session.flush()
        Session.commit()

        return "{}"

    def disable_source(self, el_id, commit=True):
        Session.query(Source).filter(Source.id == el_id)\
            .update({"state": Source.State.DISABLED})
        if commit:
            Session.commit()

    def active_source(self, el_id, commit=True):
        Session.query(Source).filter(Source.id == el_id)\
            .update({"state": Source.State.ACTIVE})
        if commit:
            Session.commit()

    def delete_source(self, el_id, commit=True):
        m = Source.Key(el_id)
        Session.delete(m)
        if commit:
            Session.flush()
            Session.commit()

    def apply_filters(self, start=None, count=None, search=None):
        q = Session.query(Source, Unit, Application, Account)\
            .filter(Account.id == Application.account)\
            .filter(Application.id == Unit.application)\
            .filter(Unit.id == Source.unit)\
            .order_by(Source.id.desc())

        if getattr(g, 'account', None) and g.account.isdigit():
            q = q.filter(Account.id == g.account)

        if getattr(g, 'application', None) and g.application.isdigit():
            q = q.filter(Application.id == g.application)

        if getattr(g, 'unit', None) and g.unit.isdigit():
            q = q.filter(Unit.id == g.unit)

        if getattr(g, 'tab', None):
            q = {
                Tabs.NEW: q.filter(Source.state == Source.State.NONE),
                Tabs.ACTIVE: q.filter(Source.state == Source.State.ACTIVE),
                Tabs.BANNED: q.filter(Source.state == Source.State.DISABLED),
            }.get(g.tab)

        if getattr(g, 'source_type', None) and g.source_type is not None:
            q = q.filter(Source.type == g.source_type)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    Application.name.ilike(u'%{0}%'.format(search)),
                    Unit.name.ilike(u'%{0}%'.format(search)),
                    Source.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Source.id == search,
                ))
            else:
                q = q.filter(or_(
                    Application.name.ilike(u'%{0}%'.format(search)),
                    Account.email.ilike(u'%{0}%'.format(search)),
                    Unit.name.ilike(u'%{0}%'.format(search)),
                    Source.name.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        g.data = q.all()
        g.total = query_total.count()

    def _build_dicts(self, select=None, search=None):
        """Builds dictionaries."""
        q = Session.query(Account).order_by(Account.id)
        if select == 'account':
            q = q.filter(Account.email.ilike(u'%{0}%'.format(search)))
        g.accounts = q.all()

        q = Session.query(Application)
        if select == 'application':
            q = q.filter(Application.name.ilike(u'%{0}%'.format(search)))
        g.applications = q.all()

        q = Session.query(Unit)
        if select == 'unit':
            q = q.filter(Unit.name.ilike(u'%{0}%'.format(search)))
        g.units = q.all()

        g.accounts_dict = dict([(account.id, (account.email, u''))
                               for account in g.accounts])
        g.applications_dict = dict([(app.id, (app.name, app.account))
                                   for app in g.applications])
        g.units_dict = dict([(u.id, (u.name, u.application)) for u in g.units])

    # This method came from admin statistics.
    def _build_pipeline(self, pipeline_elements, light=False, select=None,
                        search=None):
        pipeline = {}
        if not light:
            self._build_dicts(select=select, search=search)

        store = {
            'account': {
                'name': 'account',
                'id': 'select_account',
                'dict': None if light else g.accounts_dict,
            },
            'application': {
                'name': 'application',
                'id': 'select_application',
                'dict': None if light else g.applications_dict,
            },
            'unit': {
                'name': 'unit',
                'id': 'select_unit',
                'dict': None if light else g.units_dict,
            },
        }

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline