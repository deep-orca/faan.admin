# -*- coding: utf-8 -*-
import json
import time
from calendar import timegm
from operator import itemgetter
from datetime import datetime
from flask import g, request, redirect, jsonify
from sqlalchemy import or_
from sqlalchemy.sql.expression import desc
from faan.admin.controllers import permissions
from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template
from faan.core.model.security import Account
from faan.x.security import CProtect
from faan.admin.lib.security import IsSigned
from faan.x.util.func import try_int
from faan.x.util.timestamp import TimeStamp
from faan.core.model.general.sdklog import SDKLog
from faan.core.model.meta import Session
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model.pub.application import Application


class SDKLogControllerError(Exception):
    pass


@CProtect(IsSigned())
# @CProtect(permissions.sdk)
@Controller("/sdk")
class SDKLogController():

    def _build_dicts(self, select=None, search=None):
        """Builds dictionaries."""

        q = Session.query(Account).order_by(Account.username)
        if select == 'publisher':
            q = q.filter(Account.username.ilike(u'%{0}%'.format(search)))
        g.publishers = q.all()

        if select == 'advertiser':
            q = q.filter(Account.username.ilike(u'%{0}%'.format(search)))
        g.advertisers = q.all()

        q = Session.query(Campaign).order_by(Campaign.name)
        if select == 'campaign':
            q = q.filter(Campaign.name.ilike(u'%{0}%'.format(search)))
        g.campaigns = q.all()

        q = Session.query(Media).order_by(Media.name)
        if select == 'media':
            q = q.filter(Media.name.ilike(u'%{0}%'.format(search)))
        g.medias = q.all()

        q = Session.query(Application).order_by(Application.name)
        if select == 'application':
            q = q.filter(Application.name.ilike(u'%{0}%'.format(search)))
        g.applications = q.all()

        g.publishers_dict = dict([(account.id, (account.username, None))
                                 for account in g.publishers])

        g.advertisers_dict = dict([(account.id, (account.username, None))
                                  for account in g.advertisers])

        g.applications_dict = dict([(app.id, (app.name, app.account))
                                    for app in g.applications])

        g.campaigns_dict = dict([(c.id, (c.name, c.account))
                                 for c in g.campaigns])
        g.medias_dict = dict([(m.id, (m.name, m.campaign))
                              for m in g.medias])


    def _build_pipeline(self, pipeline_elements, light=False, select=None,
                        search=None):
        """Builds pipeline from 'select' elements. The pipeline means that
        elements dependencies each other."""
        pipeline = {}
        if not light:
            self._build_dicts(select=select, search=search)

        store = {
            'publisher': {
                'name': 'publisher',
                'header': u'Publisher',
                'id': 'select_pub',
                'dict': None if light else g.publishers_dict,
                'help_string': 'Publisher',
            },
            'advertiser': {
                'name': 'advertiser',
                'header': u'Advertiser',
                'id': 'select_adv',
                'dict': None if light else g.advertisers_dict,
                'help_string': 'Advertiser',
            },
            'campaign': {
                'name': 'campaign',
                'header': u'Campaign',
                'id': 'select_campaign',
                'dict': None if light else g.campaigns_dict,
                'help_string': 'Campaign',
            },
            'media': {
                'name': 'media',
                'header': u'Media',
                'id': 'select_media',
                'dict': None if light else g.medias_dict,
                'help_string': 'Media',
            },
            'application': {
                'name': 'application',
                'header': u'Application',
                'id': 'select_app',
                'dict': None if light else g.applications_dict,
                'help_string': 'Application',
            },
        }

        for _ in pipeline_elements:
            pipeline[_] = store[_]
        return pipeline
    
    @Route("/log/")
    def log(self):
        today = datetime.fromtimestamp(TimeStamp.today()).strftime('%d.%m.%Y')
        g.publisher = request.values.get('publisher', '')
        g.advertiser = request.values.get('advertiser', '')
        g.campaign = request.values.get('campaign', '')
        g.media = request.values.get('media', '')
        g.application = request.values.get('application', '')
        g.err_type = request.values.get('err_type', '')
        g.dev_type = request.values.get('dev_type', '')
        g.platform_type = request.values.get('platform_type', '')
        g.date_period = request.values.get('date', '') or today + ' - ' + today

        elements = ['advertiser', 'publisher', 'campaign', 'media',
                    'application']

        pipeline = self._build_pipeline(elements, light=True)
        g.selects = [pipeline[e] for e in elements]

        return render_template('sdk/log.mako')

    @Route("/log/ajax/select")
    def ajax_select(self):
        query = request.values.get('query', '')
        values = request.values.get('values', '')
        search = request.values.get('q', '')
        obj_list = [{'id': None, 'name': u""}]

        if query in ('advertiser', 'campaign', 'media',):
            v = values.split(',')
            values = ','.join([s for s in v if not s.startswith('application')
                              and not s.startswith('publisher')])
        elif query in ('publisher', 'application',):
            v = values.split(',')
            values = ','.join([s for s in v if not s.startswith('campaign')
                              and not s.startswith('media')
                              and not s.startswith('advertiser')])

        pipeline_elements = map(lambda x: x.split(':')[0], values.split(','))
        pipeline = self._build_pipeline(pipeline_elements, select=query,
                                        search=search)
        allowed = 'all'
        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            try:
                select = pipeline[name]
            except KeyError:
                break
            if name == query:
                if allowed == 'all':
                    obj_list += [{'id': key, 'name': n + " (%s)" % key}
                                 for key, (n, _) in select['dict'].items()]
                else:
                    obj_list += [{'id': key, 'name': n + " (%s)" % key}
                                 for key, (n, p) in select['dict'].items()
                                 if p in allowed]
                break
            else:
                if _id and _id.isdigit():
                    allowed = [int(_id)]
                elif allowed != 'all':
                    allowed = [int(key)
                               for key, (_, p) in select['dict'].items()
                               if p in allowed]
        return jsonify(**{"objects": obj_list})

    def _init_date(self, timezone=None):
        if not timezone:
            timezone = -time.timezone
        try:
            g.date_period = request.values['date']
            g.ts, g.te = request.values['date'].split(' - ')
            g.ts = timegm(datetime.strptime(g.ts, "%d.%m.%Y").timetuple())
            g.te = timegm(datetime.strptime(g.te, "%d.%m.%Y").timetuple())
            g.ts, g.te = g.ts - timezone, g.te - timezone
        except StandardError:
            g.ts = g.te = TimeStamp.today() - timezone
            today = datetime.utcfromtimestamp(g.ts).strftime('%d.%m.%Y')
            g.date_period = today + ' - ' + today

    def _filter_sdk(self, _filter=None, start=None, count=None, search=None,
                    timezone=None):
        self._init_date(timezone=timezone)

        q = Session.query(SDKLog).order_by(SDKLog.timestamp.desc(),
                                           SDKLog.id.desc())
        q = q.filter(SDKLog.timestamp >= g.ts) \
             .filter(SDKLog.timestamp < g.te + TimeStamp.DAYSECONDS - 1)

        if g.publisher and g.publisher.isdigit():
            applications = Session.query(Application.id) \
                .filter(Application.account == g.publisher).all()
            applications = [_id for (_id,) in applications]
            if applications:
                q = q.filter(SDKLog.application.in_(applications))
            else:
                g.data = []

        if g.advertiser and g.advertiser.isdigit() and not hasattr(g, 'data'):
            campaigns = Session.query(Campaign.id) \
                .filter(Campaign.account == g.advertiser).all()
            campaigns = [_id for (_id,) in campaigns]
            if campaigns:
                q = q.filter(SDKLog.campaign.in_(campaigns))
            else:
                g.data = []

        if g.campaign and g.campaign.isdigit():
            q = q.filter(SDKLog.campaign == g.campaign)

        if g.media and g.media.isdigit():
            q = q.filter(SDKLog.media == g.media)

        if g.application and g.application.isdigit():
            q = q.filter(SDKLog.application == g.application)

        if g.error_type and g.error_type.isdigit() and int(g.error_type):
            q = q.filter(SDKLog.type == g.error_type)

        if g.device_type and g.device_type.isdigit() and int(g.device_type):
            q = q.filter(SDKLog.devclass == g.device_type)

        if g.platform_type and g.platform_type.isdigit() \
                and int(g.platform_type):
            q = q.filter(SDKLog.platform == g.platform_type)

        if _filter and callable(_filter):
            q = _filter(q)

        if search:
            if search.isdigit():
                q = q.filter(or_(
                    SDKLog.message.ilike(u'%{0}%'.format(search)),
                    SDKLog.context.ilike(u'%{0}%'.format(search)),
                    SDKLog.id == search,
                    SDKLog.application == search,
                    SDKLog.campaign == search,
                    SDKLog.media == search,
                    SDKLog.geo == search,
                    SDKLog.sdkv == search
                ))
            else:
                q = q.filter(or_(
                    SDKLog.message.ilike(u'%{0}%'.format(search)),
                    SDKLog.context.ilike(u'%{0}%'.format(search)),
                ))

        query_total = q

        if start and count:
            q = q.offset(start)
            q = q.limit(count)

        if not hasattr(g, 'data'):
            g.data = q.all()
            g.total = query_total.count()
        else:
            g.total = 0

    @Route("/log/ajax/table")
    def ajax_table(self):
        values = request.values.get('values', '')
        g.error_type = request.values.get('error_type')
        g.device_type = request.values.get('dev_type')
        g.platform_type = request.values.get('platform_type')
        start = request.values.get('start')
        count = request.values.get('count')
        search = request.values.get('search')
        timezone = try_int(request.values.get('timezone'), -time.timezone)

        for name, _id in map(lambda x: tuple(e for e in x.split(':')),
                             values.split(',')):
            setattr(g, name, _id if _id and _id.isdigit() else None)

        self._filter_sdk(start=start, count=count, search=search,
                         timezone=timezone)
        objects = []
        for entry in g.data:
            date = datetime.utcfromtimestamp(int(entry.timestamp) + timezone)\
                .strftime('%Y.%m.%d %H:%M:%S')
            d = {
                'date': date,
                'id': entry.id,
                'type': json.dumps({'id': entry.type}),
                'app': entry.application,
                'campaign': entry.campaign,
                'media': entry.media,
                'platform': json.dumps({'id': entry.platform}),
                'device': json.dumps({'id': entry.devclass}),
                'geo': entry.geo,
                'version': entry.sdkv,
                'text': json.dumps({'msg': entry.message}),
                'context': entry.context,
            }
            objects.append(d)

        data = {'meta': {'total': g.total}, 'data': objects}
        objects.sort(key=itemgetter('date'), reverse=True)

        return jsonify(**data)