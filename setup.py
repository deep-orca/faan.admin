import os
from setuptools import setup as xsetup, find_packages

x_pkg_name = os.path.basename(os.getcwd())
print x_pkg_name

xsetup(
    name='faan.admin',
    version='0.0.1',
    description='faan admin',
    author='',
    author_email='',
    url='',
    namespace_packages=['faan'],
    install_requires=[
        "SQLAlchemy",
        "Flask",
        "recaptcha-client"
    ],
    setup_requires=[],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    test_suite='nose.collector',
    zip_safe=False,
    paster_plugins=['PasteScript', 'Flask'],
    entry_points="""
    [paste.app_factory]
    main = faan.admin.app:make_app
    """
)

