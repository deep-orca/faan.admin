import os
from logging import Logger

from paste.deploy import loadapp


application = loadapp('config:' + os.path.dirname(__file__) + '/configurations/development.ini')
logger = Logger(__name__)


def serve(port=8099):
    try:
        logger.info('trying gevent')
        from gevent import wsgi                             #@UnresolvedImport

        wsgi.WSGIServer(('127.0.0.1', port), application, spawn=None).serve_forever()
    except:
        logger.info('trying paste')
        from paste.httpserver import serve

        serve(application, 'localhost', port)


if __name__ == "__main__":
    serve()
